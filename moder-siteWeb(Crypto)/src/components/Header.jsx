
import logo from '../img/logo.svg'
import MobailNav from './MobailNav'
import DesktopNav from './DesktopNav'
import { NavLink } from "react-router-dom";
const Header = function () {
    return (
        <header className="header">
            <NavLink to='/'>
                <div className="header_logo_container">
                    <img alt='логотип сайта' src={logo} />
                    <div className="header_logo_text">
                        <span style={{ color: '#13C2C2' }}>Modern</span> Strategies
                    </div>
                </div>
            </NavLink>
            <nav className='nav'>
                <MobailNav />
                <DesktopNav />
            </nav>
        </header>
    );
}
export default Header


