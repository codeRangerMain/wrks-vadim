import { NavLink } from "react-router-dom";
import { Link, scroller } from "react-scroll";
import { NavLink as RouterNavLink, } from 'react-router-dom'
import { useAuth0 } from "@auth0/auth0-react";
import { useState } from "react";
import { useEffect } from "react";
const MobailNav = function () {
  const {
    isAuthenticated,
    loginWithRedirect,
  } = useAuth0();
  const menuOpen = 'chacked'
  const menuClose = ''
  const [isMenuOpen, setIsMenuOpen] = useState(menuClose)

  const menuTogle = () => {
    if (isMenuOpen === menuOpen) {
      setIsMenuOpen(menuClose)
    }
    else {
      setIsMenuOpen(menuOpen)
    }
  }
  const clickHandler = (e) => {
    let target = e.target.innerText
    menuTogle()
    let scrollTo = ''
    if (target === 'О компании') {
      scrollTo = 'aboutCompany'
    }
    if (target === 'Как это работает') {
      scrollTo = 'howItWork'
    }
    if (target === 'Тарифы') {
      scrollTo = 'tairffs'
    }
    if (target === 'Риски') {
      scrollTo = 'risks'
    }
    if (target === 'Примеры') {
      scrollTo = 'examples'
    }
    if (target === 'Контакты') {
      scrollTo = 'leedForm'
    }
    setTimeout(() => {
      scroller.scrollTo(scrollTo, {
        duration: 1000,
        smooth: true,
        offset: 0,
        spy: true
      })
    }, 100);
  }




  return (
    <div className="hamburger-menu">
      <input onClick={menuTogle} checked={isMenuOpen} id="menu__toggle" type="checkbox" />
      <label className="menu__btn" for="menu__toggle">
        <span></span>
      </label>
      <ul className="menu__box">
        <li><Link onClick={clickHandler}
          activeClass="nav-item-active"
          to="aboutCompany"
          spy={true}
          smooth={true}
          duration={500}
          className="" ><NavLink to='/' className="menu__item" >Как это работает</NavLink></Link></li>
        <li><Link onClick={clickHandler}
          activeClass="nav-item-active"
          to=""
          spy={true}
          smooth={true}
          duration={500}
          offset={-50}
        ><NavLink to='/' className="menu__item" >Тарифы</NavLink></Link></li>
        <li><Link onClick={clickHandler}
          activeClass="nav-item-active"
          to=""
          spy={true}
          smooth={true}
          duration={500}
          className="" ><NavLink to='/' className="menu__item" >Примеры</NavLink></Link></li>
        <li><Link className=''> <NavLink activeClassName="menu__item-active" to='/blog' className="menu__item" >Блог</NavLink> </Link></li>
        <li> <Link className=''>  <NavLink activeClassName="menu__item-active" to='/FAQ' className="menu__item" >FAQ</NavLink></Link></li>
        <li> <Link className=''>  <NavLink activeClassName="menu__item-active" to='/statistics' className="menu__item" >Статистика</NavLink></Link></li>
        {isAuthenticated && (

          <NavLink
            tag={RouterNavLink}
            to="/ProfilePage"
            activeClassName="menu__item-active"
            className="menu__item"
            exact
          >
            Личный кабинет
          </NavLink>
        )}

        {!isAuthenticated && (

          <li> <a
            className="menu__item"
            onClick={() => loginWithRedirect()}
          >
            Войти
          </a>
          </li>
        )}

        {isAuthenticated && (
          ''
        )

        }
        <li><Link onClick={clickHandler}
          activeClass="nav-item-active"
          to=""
          spy={true}
          smooth={true}
          duration={500}
          className="" ><NavLink to='/' className="menu__item menu__item__registration" >Контакты</NavLink></Link></li>
      </ul>
    </div>
  );
}
export default MobailNav


