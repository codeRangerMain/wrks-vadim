import '../../../App.css'
import Header from '../../Header'
import NavHeader from '../../NavHeader'
import Loading from '../../Loading'
import Footer from '../../Footer'
import { Typography } from 'antd'
import { useDispatch, useSelector } from 'react-redux'
import { useEffect } from 'react'
import { fetchNewsPage } from '../BlogPage/blogSlice'
import { useParams } from 'react-router-dom'

const { Title, Paragraph } = Typography

function BlogNewsPage() {
  const id = useParams().id
  const dispatch = useDispatch()
  const news = useSelector(state => state.blogSlice.newsPageData)
  const time = new Date(news.createdAt)
  const date = time.toLocaleDateString()
  const hours = time.getHours().toLocaleString()
  const minutes = time.getMinutes().toLocaleString()
  let isLoading = true
  useEffect(() => {
    dispatch(fetchNewsPage(id))
  }, [dispatch, id])
  if (news.length !== 0) {
    isLoading = false
  }
  return (
    <div className="blog-news-page">
      <div style={{ marginBottom: '50px', height: '200px' }} className="page_header_bg">
        <Header />
        <NavHeader name="Новость" path="/blog" toPage="Назад" />
      </div>
      <div className="content_wrapper">
        {!isLoading ?
          <div className="news-page-content">
            <img alt='Приложенное фото' className="news-page-img" src={news.photoLink} />
            <Typography className="news-page-text">
              <Title className="news-page-title" level="1">{news.header}</Title>
              <div style={{ marginTop: '0px' }} className="news-card-date">{hours} : {minutes} | {date}</div>
              <Paragraph>
                {news.fullText}
              </Paragraph>
            </Typography>
          </div> : <Loading />
        }
      </div>
      <Footer />
    </div>

  )
}

export default BlogNewsPage
