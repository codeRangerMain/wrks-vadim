import '../../../App.css'
import Header from '../../Header'
import NavHeader from '../../NavHeader'
import Footer from '../../Footer'
import NewsCard from './component/NewsCard'
import serchIco from '../../../img/searchIco.svg'
import { Pagination } from 'antd'
import { useDispatch, useSelector } from 'react-redux'
import { useEffect, useState } from 'react'
import { fetchBlog } from './blogSlice'
import Loading from '../../Loading'

function BlogPage() {
  const blogCard = useSelector((state) => state.blogSlice.blogData)
  const status = useSelector((state) => state.blogSlice.status)
  const totalCount = useSelector(state => state.blogSlice.totalCount)
  const loading = useSelector(state => state.blogSlice.loading)
  // const [cardsPerPage] = useState(20)
  const dispatch = useDispatch()
  const [page, setPage] = useState(1)
  const handleChange = page => {
    setPage(page)
  }
  const [text, setText] = useState('')
  const filteredTextChange = (e) => {
    setText(e.target.value)
  }

  const filteredNews = blogCard.filter(news => {
    return news.header.toLowerCase().includes(text.toLowerCase())
  })

  useEffect(() => {
    dispatch(fetchBlog({ page: page }))
  }, [dispatch, page])

  return (
    <div className="home_page">
      <div style={{ marginBottom: '50px', height: '200px' }} className="page_header_bg">
        <Header />
        <NavHeader name="Блог" path="/" toPage="На главную" />
        <div className="content_wrapper">

        </div>
      </div>
      <div className="content_wrapper">
        {!loading ?
          <div className="blog-content">
            {status === '200' ?
              <div className="blog-posts">
                {filteredNews.map(card => {
                  return <NewsCard key={card.id} card={card} status={status} />
                })}
                <Pagination className="blog-pagination" hideOnSinglePage defaultCurrent={page} total={totalCount}
                  onChange={handleChange} />
              </div> : ''
            }
            <div className="serch-post-container">
              <div>
                <input className="serch-posts" type="text" placeholder="Поиск" onChange={filteredTextChange} />
                <img alt="" style={{
                  position: 'relative',
                  left: '-40px'
                }} src={serchIco}
                />
              </div>
            </div>
          </div>
          : <Loading />
        }

      </div>

      <Footer />
    </div>

  )
}

export default BlogPage