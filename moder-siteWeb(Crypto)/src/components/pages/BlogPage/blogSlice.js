import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { api } from '../../../Store/api'

export const fetchBlog = createAsyncThunk(
  'blog/fetchBlog',
  async ({ page }) => {
    const response = await api.get(`/allArticles/10/${page}/0`)
    return response.data
  }
)

export const fetchNewsPage = createAsyncThunk(
  'blog/newsPage',
  async (id) => {
    const response = await api.get(`/article/${id}`)
    return response.data
  }
)

export const fetchCategory = createAsyncThunk(
  'blog/fetchCategory',
  async () => {
    const response = await api.get(`/allArticles/byCategory/1/10/1/0`)
    return response.data
  }
)


const blogSlice = createSlice({
  name: 'blog',
  initialState: {
    blogData: [],
    newsPageData: [],
    categoryData: [],
    status: '',
    totalCount: '',
    loading: true,
    newsPageLoading: false,
    error: {
      failed: false,
      message: null
    }
  },
  extraReducers: {
    [fetchBlog.pending]: (state) => {
      state.loading = true
      state.error.failed = false
    },
    [fetchBlog.fulfilled]: (state, action) => {
      state.loading = false
      state.blogData = action.payload.data.articlesList
      state.totalCount = action.payload.data.totalCount
      state.status = action.payload.status
    },
    [fetchBlog.rejected]: (state, action) => {
      state.loading = true
      state.error.failed = true
      state.error.message = action.payload
    },
    [fetchNewsPage.pending]: (state) => {
      state.newsPageLoading = true
      state.error.failed = false
    },
    [fetchNewsPage.fulfilled]: (state, action) => {
      state.newsPageLoading = false
      state.newsPageData = action.payload.data
    },
    [fetchNewsPage.rejected]: (state, action) => {
      state.newsPageLoading = false
      state.error.failed = true;
      state.error.message = action.payload;
    },
    [fetchCategory.pending]: (state) => {
      state.newsPageLoading = true
      state.error.failed = false
    },
    [fetchCategory.fulfilled]: (state, action) => {
      state.newsPageLoading = false
      state.categoryData = action.payload.data
    },
    [fetchCategory.rejected]: (state, action) => {
      state.newsPageLoading = false
      state.error.failed = true;
      state.error.message = action.payload;
    },
  }
})

export default blogSlice.reducer