
import { useHistory } from 'react-router-dom';


function NewsCard({ card }) {
  const history = useHistory()
  const time = new Date(card.createdAt);
  const date = time.toLocaleDateString()
  const hours = time.getHours().toLocaleString()
  const minutes = time.getMinutes().toLocaleString()

  const handleClick = () => {
    history.push(`/newsPage/${card.id}`)
  }


  if (card) {
    return (
      <div className="news-card">
        <img alt='Прилагающиеся фото' className="news-card-img" src={card.photoLink} />
        <div className='news-card-content'>
          <h2 className='news-card-title'>
            {card.header}</h2>
          <div className='news-card-date'>{hours} : {minutes} | {date}</div>
          <p className='news-card-text'>{card.previewText}</p>
          <button onClick={handleClick} className='btn blog-btn' >Читать далее</button>
        </div>
      </div>

    );
  } else {
    return ''
  }



}

export default NewsCard;
