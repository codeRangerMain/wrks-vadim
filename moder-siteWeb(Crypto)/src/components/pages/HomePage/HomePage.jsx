
import '../../../App.css'
import { useTranslation } from 'react-i18next'
import Header from '../../Header'
import { Typography } from 'antd'
import EarningInfo from './components/EarningInfo/EarningInfo';
import AboutCompany from './components/AboutCompany';
import HowItWork from './components/HowItWork';
import Footer from '../../Footer';
import Tairffs from './components/Tariffs';
import Examples from './components/Examples';
import Calculator from './components/Calculator/Calculator'
import { useDispatch, useSelector } from 'react-redux'
import { useEffect } from 'react'
import { fetchEarningInfo } from './components/EarningInfo/earningInfoSlice'
import { Link, } from "react-scroll";
import ReffCalculator from './components/ReffCalculator/ReffCalculator'
import LeedForm from './components/LeedForm/LeedForm'
import ComparisonTable from './components/ComparisonTable';
import { useLocation } from 'react-router-dom';
const { Title, Paragraph, } = Typography;


function HomePage() {
    const dispatch = useDispatch()
    const projectBalance = useSelector((state) => state.earningInfoSlice.amount)
    const refLink = useLocation().search
    const {t} = useTranslation()

    useEffect(() => {
        if (refLink.length > 0) {
            localStorage.setItem('refLink', refLink)
        }
    }, [refLink])

    useEffect(() => {
        dispatch(fetchEarningInfo())
    }, [dispatch])

    return (

        <div className="home_page">
            <div className='page_header_bg'>
                <Header />
                <div className='content_wrapper'>
                    <div className='home_page_title_btn'>
                        <Typography style={{ marginTop: '120px' }} >
                            <Title level='1' style={{ fontSize: '28px', color: '#fff', fontFamily: 'PT Sans, sans-serif' }}>{t("title")}
                                <span style={{ color: '#13C2C2' }}> {t("titlePresents")}</span> </Title>
                            <Paragraph style={{ fontSize: '18px', color: '#627377', fontFamily: 'PT Sans, sans-serif', lineHeight: 1.2, fontWeight: 700, marginBottom: '46px' }}>
                                {t("supTitle")}
                            </Paragraph>
                        </Typography>
                        <Link
                            to="leedForm"
                            spy={true}
                            smooth={true}
                            duration={500}
                            className="btn btn-header">{t("learnMore")}</Link>
                    </div>
                    <EarningInfo projectBalance={projectBalance} />

                </div>
            </div>
            <div className='content_wrapper'>
                <AboutCompany />
                <Calculator />
                <ComparisonTable t={t} />
                <HowItWork t={t} />
            </div>
            <Tairffs t={t}/>
            <div style={{ maxWidth: '1300px' }} className='content_wrapper'>
                <LeedForm t={t}/>
                {/*<ReffCalculator />*/}
                <Examples t={t}/>
            </div>
            <Footer />
        </div>
    );
}

export default HomePage;
