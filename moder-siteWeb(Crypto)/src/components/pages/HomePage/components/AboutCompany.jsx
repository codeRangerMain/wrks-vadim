import { Element } from 'react-scroll'
import { useTranslation } from 'react-i18next'

function AboutCompany() {
  const {t} = useTranslation()

  return (

        <Element name='aboutCompany' style={{ marginTop: '70px' }} className="about_company">
            <div className="about_company_item">
                <h1 className="secttion_title">
                  {t("ourMission")}
                </h1>
                <p className="about_company_text">
                  {t("ourMissionText")}
                </p>
            </div>

            {/*<div className="about_company_item" style={{ marginTop: '53px', border: '2px solid #D0D0D0', borderRadius: '10px', padding: '20px', backgroundColor: '#fff' }}>*/}
            {/*    <h2 className="about_company_title">*/}
            {/*        Что такое DeFi?*/}
            {/*    </h2>*/}
            {/*    <p className="about_company_text">*/}
            {/*        От английского, decentralized finance, или же, по-русски, децентрализованные финансы. Этим термином обозначаются все проекты в криптовалютной индустрии, которые управляются напрямую пользователями этих проектов.*/}
            {/*    </p>*/}
            {/*</div>*/}

            {/*<div className="about_company_item" style={{ marginTop: '53px' }}>*/}
            {/*    <p className="about_company_text">*/}
            {/*        Например, биржа Binance (крупнейшая крипто-биржа в мире), централизована, так как имеет свое собственное руководство и все решения принимаются только им. В то же время криптовалютный обменник Uniswap (крупнейший DeFi обменник), полностью децентрализован и все решения принимаются пользовательским сообществом посредством голосований.*/}
            {/*    </p>*/}
            {/*</div>*/}
            <div className="about_company_item" style={{ marginTop: '53px', border: '2px solid #13c2c2', borderRadius: '10px', padding: '20px', backgroundColor: '#fff' }}>
                <h2 className="about_company_title">
                  {t("fundActivitiesTitle")}
                </h2>
                <p className="about_company_text">
                  {t("fundActivities")}
                </p>
            </div>
            <div className="about_company_item" style={{ marginTop: '53px' }}>
                <p className="about_company_text">
                  {t("calcPrevText")}
                </p>
            </div>
        </Element >
    );
}

export default AboutCompany;
