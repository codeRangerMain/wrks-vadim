import React, { useState } from 'react'
import { Checkbox, Slider } from 'antd'
import 'antd/dist/antd.css'
import CalculatorCards from './CalculatorCards'
import { useTranslation } from 'react-i18next'


function Calculator() {
  const [value, setValue] = useState(3000)
  const handleChange = (value) => {
    setValue(value)
  }
  const [checked, setChecked] = useState(true)
  const handleCheckboxChange = () => {
    setChecked(!checked)
  }
  const {t} = useTranslation()
  const pricingPlans = [
    {
      id: 1,
      name: `${t("beginner")}`,
      monthlyFormula: (value * 0.15 + (value * 0.15 / 2) * 0.15) / 12,
      yearlyFormula: value * 0.15 + (value * 0.15 / 2) * 0.15,
      monthlyWithoutProlongation: value * 0.15 / 12,
      yearlyWithoutProlongation: value * 0.15,
      color: '#849FA0',
      accruals: 'Раз в неделю',
      procent: 'до 15%'
    },
    {
      id: 2,
      name: `${t("advanced")}`,
      monthlyFormula: (value * 0.19 + (value * 0.19 / 2) * 0.19) / 12,
      yearlyFormula: value * 0.19 + (value * 0.19 / 2) * 0.19,
      monthlyWithoutProlongation: value * 0.19 / 12,
      yearlyWithoutProlongation: value * 0.19,
      color: '#13C2C2',
      accruals: 'Раз в неделю',
      procent: 'до 19%'
    },
    // {
    //   id: 3,
    //   name: 'Профессиональный',
    //   monthlyFormula: (value * 2 + (value * 2 / 2) * 2) / 12,
    //   yearlyFormula: value * 2 + (value * 2 / 2) * 2,
    //   monthlyWithoutProlongation: value * 2 / 12,
    //   yearlyWithoutProlongation: value * 2,
    //   color: '#EF5B1C',
    //   accruals: 'Каждый день'
    // }
  ]
  return (
    <section className='calculator'>
      <div >
        <h2 className='secttion_title'>{t("calcTitle")}</h2>
        <Checkbox checked={checked} onChange={handleCheckboxChange}><div className='calculator-input-text'>{t("checkboxText")}</div></Checkbox>
        <div className='calculator-slider-input'>
          <Slider
            color={'red'}
            min={300}
            max={30000}
            onChange={handleChange}
            value={typeof value === 'number' ? value : 0}
          />
          <div className='calculator-сontribution' >{t("depositAmount")} {value} <span style={{ fontSize: '25px', right: '0' }} className='dollar'>$</span></div>
        </div>
      </div>
      <div className='calculator-item-container'>
        {pricingPlans.map(data => {
          return <CalculatorCards key={data.id} data={data} checked={checked} t={t} />
        })}
      </div>

    </section>
  )
}

export default Calculator
