import React from 'react'
import { Tooltip } from 'antd'
import { Link } from 'react-scroll'
import warningImg from '../../../../../img/warning.svg'


function CalculatorCards({ data, checked, t}) {

  return (
    <div style={{ position: 'relative' }} className='calculator_item'>
      {/*{data.id === 3 ?*/}
      {/*  <div style={{ position: 'absolute', right: '10px', top: '10px' }}>*/}
      {/*    <Tooltip title="Конкретно в этом тарифе возможна очень высокая доходность, однако присутствует вероятность уменьшения тела депозита, инвестируйте в этот тариф на свой страх и риск!">*/}
      {/*      <img style={{ width: '30px' }} src={warningImg} alt="" />*/}
      {/*    </Tooltip>*/}
      {/*  </div> : ''}*/}
      <div style={{ color: `${data.color}` }} className='calculator_item_title'>{data.name}</div>
      {checked ?
        <div>
          <div className="calculator_item-text">{t("monthlyIncome")} <div className='calculator-item-price'>{` ${Math.ceil(data.monthlyFormula)}`}<span className='dollar'>$</span></div> </div>
          <div className="calculator_item-text">{t("yearlyIncome")} <div className='calculator-item-price'>{` ${Math.ceil(data.yearlyFormula)}`}<span className='dollar'>$</span></div> </div>
          <div className="calculator_item-text">{t("expectedGrowth")} <div style={{ fontSize: '18px' }} className='calculator-item-price'>{data.procent}</div> </div>
        </div>

        :
        <div>
          <div className="calculator_item-text">{t("monthlyIncome")}  <div className='calculator-item-price'>{` ${Math.ceil(data.monthlyWithoutProlongation)}`}<span className='dollar'>$</span></div> </div>
          <div className="calculator_item-text">{t("yearlyIncome")}  <div className='calculator-item-price'>{` ${Math.ceil(data.yearlyWithoutProlongation)}`} <span className='dollar'>$</span> </div > </div>
          <div className="calculator_item-text">{t("expectedGrowth")} <div style={{ fontSize: '18px' }} className='calculator-item-price'>{data.procent}</div> </div>
        </div>
      }
      <a
        activeClass="nav-item-active"
        href="/ProfilePage"
        spy={true}
        smooth={true}
        duration={500} style={{ width: '155px', alignSelf: 'center', marginTop: '80px' }} className='btn'>{t("chooseButton")}</a>
    </div>
  )
}

export default CalculatorCards

