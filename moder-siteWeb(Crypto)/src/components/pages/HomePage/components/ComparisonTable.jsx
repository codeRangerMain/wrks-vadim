import { Element } from 'react-scroll'
import check from '../../../../img/check.svg'
import checkUnKAWAI from '../../../../img/checkUnKAWAI.svg'
import unCheck from '../../../../img/unCheck.svg'
import unCheckKAWAI from '../../../../img/unCheckKAWAI.svg'
import logo from '../../../../img/logo.svg'
import { useTranslation } from 'react-i18next'
function ComparisonTable({ t }) {

    return (
        <Element style={{ width: '100%', marginBottom: '50px', marginTop: '70px' }} name='comparisonTable' className='comparisonTable'>
            <h1 className="secttion_title">
                {t("comparisonTable")}
            </h1>
            <table style={{ color: '#000', width: '100%' }}>
                <tr>
                    <th><img style={{ width: '30px' }} src={logo} /> </th>
                    <th style={{ color: '#13c2c2' }}>Modern Strategies</th>
                    <th>{t("banks")}</th>
                    <th>{t("ETFFunds")}</th>

                </tr>
                <tr>
                    <td>{t("contributionPercent")}</td>
                    <td>до 20%</td>
                    <td>до 1.5% <br /><a href='https://www.banki.ru/products/deposits/' className='link' target='_blank'>Банки ру</a></td>
                    <td>От -90% до +90%.<br /> Нестабильный результат</td>
                </tr>
                <tr>
                    <td>{t("possibilityToWithdraw")}</td>
                    <td><img style={{ width: '20px' }} src={check} /></td>
                    <td><img style={{ width: '20px' }} src={unCheck} /></td>
                    <td><img style={{ width: '20px' }} src={check} /></td>

                </tr>
                <tr>
                    <td>{t("inputOutputCommission")}</td>
                    <td>0%</td>
                    <td>0%</td>
                    <td>1%</td>

                </tr>
                <tr>
                    <td>{t("noCommission")}</td>
                    <td><img style={{ width: '20px' }} src={check} /></td>
                    <td><img style={{ width: '20px' }} src={unCheck} /></td>
                    <td><img style={{ width: '20px' }} src={check} /></td>

                </tr>
                <tr>
                    <td>{t("easyStart")}</td>
                    <td ><img style={{ width: '20px' }} src={check} /></td>
                    <td><img style={{ width: '20px' }} src={check} /></td>
                    <td><img style={{ width: '20px' }} src={unCheck} /></td>
                </tr>
                <tr>
                    <td>{t("guaranteedProfitability")}</td>
                    <td><img style={{ width: '20px' }} src={unCheck} /></td>
                    <td><img style={{ width: '20px' }} src={check} /></td>
                    <td><img style={{ width: '20px' }} src={unCheck} /></td>
                </tr>
            </table>
        </Element >
    );
}

export default ComparisonTable
