import React from 'react'

function ReffCalculatorCards({ data, checked }) {
  return (
    <div style={{ height: '250px', paddingTop: '60px' }} className='calculator_item'>
      <div style={{ marginBottom: '25px' }} className='calculator_item_title'>{data.name}</div>
      {checked ?
        <div>
          <div className="calculator_item-text">Доход за месяц:  <div className='calculator-item-price'>{` ${Math.ceil(data.monthlyFormula)}`}<span className='dollar'>$</span></div> </div>
          <div className="calculator_item-text">Доход за год:  <div className='calculator-item-price'>{` ${Math.ceil(data.yearlyFormula)}`}<span className='dollar'>$</span></div> </div>
        </div>
        :
        <div>
          <div className="calculator_item-text">Доход за месяц:  <div className='calculator-item-price'>{` ${Math.ceil(data.monthlyWithoutProlongation)}`}<span className='dollar'>$</span></div> </div>
          <div className="calculator_item-text">Доход за год:  <div className='calculator-item-price'>{` ${Math.ceil(data.yearlyWithoutProlongation)}`} <span className='dollar'>$</span> </div > </div>
        </div>
      }
    </div>
  )
}

export default ReffCalculatorCards

