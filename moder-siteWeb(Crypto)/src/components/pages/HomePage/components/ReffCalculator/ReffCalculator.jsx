import React, { useState } from 'react'
import { Slider } from 'antd'
import 'antd/dist/antd.css'
import ReffCalculatorCards from './ReffCalculatorCards'


function ReffCalculator() {
  const [value, setValue] = useState(3000)
  const handleChange = (value) => {
    setValue(value)
  }
  const [checked, setChecked] = useState(false)

  const pricingPlans = [
    {
      id: 1,
      name: 'Продвинутый',
      monthlyFormula: (value * 0.19 + (value * 0.19 / 2) * 0.19) / 12,
      yearlyFormula: value * 0.19 + (value * 0.19 / 2) * 0.19,
      monthlyWithoutProlongation: value * 0.05 / 12,
      yearlyWithoutProlongation: value * 0.05,
      color: '#13C2C2',
    },
    {
      id: 2,
      name: 'Профессиональный',
      monthlyFormula: (value * 2 + (value * 2 / 2) * 2) / 12,
      yearlyFormula: value * 2 + (value * 2 / 2) * 2,
      monthlyWithoutProlongation: value * 0.45 / 12,
      yearlyWithoutProlongation: value * 0.45,
      color: '#EF5B1C',
    }
  ]
  return (
    <section className='calculator'>
      <div >
        <h2 className='secttion_title'>Рассчитай свой реферальный доход:</h2>
        <div className='calculator-slider-input'>
          <Slider
            color={'red'}
            min={300}
            max={50000}
            onChange={handleChange}
            value={typeof value === 'number' ? value : 0}
          />
          <div className='calculator-сontribution' >Сумма вкладов твоих рефералов: {value} <span style={{ fontSize: '25px', right: '0' }} className='dollar'>$</span></div>
        </div>
      </div>
      <div className='calculator-item-container'>
        {pricingPlans.map(data => {
          return <ReffCalculatorCards key={data.id} data={data} checked={checked} />
        })}
      </div>

    </section>
  )
}

export default ReffCalculator