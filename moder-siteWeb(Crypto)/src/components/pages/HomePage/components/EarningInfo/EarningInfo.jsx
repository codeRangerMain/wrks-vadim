import { useTranslation } from 'react-i18next'

function EarningInfo({ projectBalance }) {
  const {t} = useTranslation()
  let balance = ''
    if (projectBalance.status === '200') {
        balance = projectBalance
    }
    else {
        balance = {
            data: {
                balancesAmount: 0,
                earnedByUsers: 0,
                // refShare: 0,
            }
        }
    }
    return (
        <section style={{ marginTop: '68px' }} className='earning_information'>
            <div className='earning_information_item'>
                <div className='earning_information_sum'>{`${Math.round(balance.data.balancesAmount)} $`} </div>
                <div className='earning_information_title'>{t("currentBalance")}</div>
            </div>
            <div className='earning_information_item'>  <span className='earning_information_sum'> 0 %</span>

                <div className='earning_information_title'>{t("commission")}</div>
            </div>
            {/*<div className='earning_information_item'>  <div className='earning_information_sum'>{balance.data.refShare + ' %'} </div>*/}
            <div className='earning_information_item'>  <div className='earning_information_sum'>20%</div>
                <div className='earning_information_title'>{t("annualReturn")}</div>
            </div>
        </section>
    );


}

export default EarningInfo;
