import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { api } from '../../../../../Store/api'

export const fetchEarningInfo = createAsyncThunk(
  'earningInfo/fetchEarningInfo',
  async () => {
    const response = await api.get('/fundStatistics')
    return response.data
  }
)

const earningInfoSlice = createSlice({
  name: 'earningInfo',
  initialState: {
    amount: {},
    loading: false,
    error: {
      failed: false,
      message: null,
    }
  },
  extraReducers: {
    [fetchEarningInfo.pending]: (state) => {
      state.loading = true;
      state.error.failed = false;
    },
    [fetchEarningInfo.fulfilled]: (state, action) => {
      state.loading = false;
      state.amount = action.payload;
    },
    [fetchEarningInfo.rejected]: (state, action) => {
      state.loading = false;
      state.error.failed = true;
      state.error.message = action.payload;
    }
  }
})

export default earningInfoSlice.reducer