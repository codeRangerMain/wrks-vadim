import arrow from '../../../../img/arrowRight.svg'
import { Element } from 'react-scroll'
import { Link } from "react-scroll";
import warningImg from "../../../../img/warning.svg"
import { Tooltip } from 'antd';


function Tairffs({ t }) {
    return (
        <Element name='tairffs' className='tairffs'>
            <div className="secttion_title">{t("tariffs")}</div>
            <div className="tairffs_container">
                <div className='tairffs_item'>
                    <div style={{ color: '#849FA0' }} className='tairffs_item_title'>{t("beginner")}</div>
                    <div className='tairffs_item_price'><span style={{ position: 'relative', top: '10px' }}>от </span> <span style={{ marginLeft: '10px', marginRight: '10px' }} className="price_bold">300</span> <span style={{ position: 'relative', fontSize: '34px', fontWeight: 'bold' }}>$</span></div>
                    <div className='tairffs_about'>
                        <div className="tairffs_about_item">  <div><img alt="" className='tairffs_item_arrow' src={arrow} />{t("yearlyIncomeWithIn")} </div> <span style={{ fontWeight: 'bold', fontSize: '20px' }}>До 15%</span></div>
                        <div style={{ marginBottom: '5px' }} className="tairffs_about_item"> <div><img alt="" className='tairffs_item_arrow' src={arrow} />{t("WalletType")}</div>  <span>{t("common")}</span></div>
                        <div className="tairffs_about_item"> <div><img alt="" className='tairffs_item_arrow' src={arrow} />{t("Accruals")}</div>  <span>{t("AccrualsType")}</span></div>
                    </div>
                    <a
                        activeClass="nav-item-active"
                        href="/ProfilePage"
                        spy={true}
                        smooth={true}
                        duration={500} style={{ width: '155px', alignSelf: 'center' }} className='btn'>{t("chooseButton")}</a>
                </div>
                <div className='tairffs_item'>
                    <div style={{ color: '#13C2C2' }} className='tairffs_item_title'>{t("advanced")}</div>
                    <div className='tairffs_item_price'><span style={{ position: 'relative', top: '10px' }}>от </span> <span style={{ marginLeft: '10px', marginRight: '10px' }} className="price_bold">750</span> <span style={{ position: 'relative', fontSize: '34px', fontWeight: 'bold' }}>$</span></div>
                    <div className='tairffs_about'>
                        <div className="tairffs_about_item">  <div><img alt="" className='tairffs_item_arrow' src={arrow} />{t("yearlyIncomeWithIn")} </div> <span style={{ fontWeight: 'bold', fontSize: '20px' }}>До 19%</span></div>
                        <div style={{ marginBottom: '5px' }} className="tairffs_about_item"> <div><img alt="" className='tairffs_item_arrow' src={arrow} />{t("WalletType")}</div>  <span>{t("common")}</span></div>
                        <div className="tairffs_about_item"> <div><img alt="" className='tairffs_item_arrow' src={arrow} />{t("Accruals")}</div>  <span>{t("AccrualsType")}</span></div>
                    </div>
                    <a
                        activeClass="nav-item-active"
                        href='/ProfilePage'
                        spy={true}
                        smooth={true}
                        duration={500}
                        style={{ width: '155px', alignSelf: 'center' }} className='btn'>{t("chooseButton")}</a>

                </div>
                {/*<div style={{ position: 'relative' }} className='tairffs_item'>*/}
                {/*    <div style={{ position: 'absolute', right: '10px', top: '10px' }}>*/}
                {/*        <Tooltip title="Конкретно в этом тарифе возможна очень высокая доходность, однако присутствует вероятность уменьшения тела депозита, инвестируйте в этот тариф на свой страх и риск!">*/}
                {/*            <img style={{ width: '30px' }} src={warningImg} alt="" />*/}
                {/*        </Tooltip>*/}
                {/*    </div>*/}
                {/*    <div style={{ color: '#EF5B1C' }} className='tairffs_item_title'>Профессиональный</div>*/}
                {/*    <div className='tairffs_item_price'><span style={{ position: 'relative', top: '10px' }}>от </span> <span style={{ marginLeft: '10px', marginRight: '10px' }} className="price_bold">150</span> <span style={{ position: 'relative', fontSize: '34px', fontWeight: 'bold' }}>$</span></div>*/}
                {/*    <div className='tairffs_about'>*/}
                {/*        <div className="tairffs_about_item">  <div><img alt="" className='tairffs_item_arrow' src={arrow} />Доходность в год </div> <span style={{ fontWeight: 'bold', fontSize: '20px' }}>до 200%</span></div>*/}
                {/*        <div style={{ marginBottom: '5px' }} className="tairffs_about_item"> <div><img alt="" className='tairffs_item_arrow' src={arrow} />Тип кошелька</div>  <span>Общий</span></div>*/}
                {/*        <div className="tairffs_about_item"> <div><img alt="" className='tairffs_item_arrow' src={arrow} />Начисления</div>  <span>Каждый день</span></div>*/}
                {/*    </div>*/}
                {/*    <Link*/}
                {/*        activeClass="nav-item-active"*/}
                {/*        to="leedForm"*/}
                {/*        spy={true}*/}
                {/*        smooth={true}*/}
                {/*        duration={500} style={{ width: '155px', alignSelf: 'center' }} className='btn'>Выбрать</Link>*/}
                {/*</div>*/}
                <div className='tairffs_item'>
                    <div style={{ color: '#2A9CCC' }} className='tairffs_item_title'>{t("large")}</div>
                    <div className='tairffs_item_price'><span style={{ position: 'relative', top: '10px' }}>от </span> <span style={{ marginLeft: '10px', marginRight: '10px' }} className="price_bold"> 15 000</span> <span style={{ position: 'relative', fontSize: '34px', fontWeight: 'bold' }}>$</span></div>
                    <div className='tairffs_about'>
                        <div className="tairffs_about_item">  <div><img alt="" className='tairffs_item_arrow' src={arrow} />{t("yearlyIncomeWithIn")} </div> <span style={{ fontWeight: 'bold', fontSize: '20px' }}>До 20%</span></div>
                        <div style={{ marginBottom: '5px' }} className="tairffs_about_item"> <div><img alt="" className='tairffs_item_arrow' src={arrow} />{t("WalletType")}</div>  <span>{t("personal")}</span></div>
                        <div className="tairffs_about_item"> <div><img alt="" className='tairffs_item_arrow' src={arrow} />{t("Accruals")}</div>  <span>{t("AccrualsType")}</span></div>
                    </div>
                    <a
                        activeClass="nav-item-active"
                        to="/ProfilePage"
                        spy={true}
                        smooth={true}
                        duration={500} style={{ width: '155px', alignSelf: 'center' }} className='btn'>{t("chooseButton")}</a>
                </div>
            </div>
        </Element>
    );
}

export default Tairffs;