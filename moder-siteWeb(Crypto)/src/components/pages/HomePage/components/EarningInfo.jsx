function EarningInfo() {
    return (

        <section style={{ marginTop: '68px' }} className='earning_information'>
            <div className='earning_information_item'>
                <div className='earning_information_sum'>1 000 000 </div>
                <div className='earning_information_title'>Вложено денег</div>
            </div>
            <div className='earning_information_item'>  <span className='earning_information_sum'>1 000 000 000 </span>
                <div className='earning_information_title'>Инвесторы заработали</div>
            </div>
            <div className='earning_information_item'>  <div className='earning_information_sum'>1 000  </div>
                <div className='earning_information_title'>Вкладчиков</div>
            </div>
        </section>
    );
}

export default EarningInfo;
