import itemImg1 from '../../../../img/howItWork1.png'
import itemImg2 from '../../../../img/howItWork2.png'
import itemImg3 from '../../../../img/howItWork3.png'
import itemImg4 from '../../../../img/howItWork4.png'
import itemImg1Num1 from '../../../../img/howItWorkNum1.svg'
import itemImg1Num2 from '../../../../img/howItWorkNum2.svg'
import itemImg1Num3 from '../../../../img/howItWorkNum3.svg'
import itemImg1Num4 from '../../../../img/howItWorkNum4.svg'
import line from '../../../../img/line.svg'
import { Typography } from 'antd'
import { Element } from 'react-scroll'

const { Title, Paragraph } = Typography

function HowItWork ({ t }) {
  return (

    <Element name="howItWork" style={{ marginTop: '70px' }} className="how_it_work">
      <img alt="" className="how_it_work_dec_line" src={line}/>
      <div className="secttion_title">{t("howItWorks")}</div>
      <div className="how_it_work_item">
        <div className="how_it_work_img_container">
          <img alt="" className="how_it_work_img" src={itemImg1}/>
          <img alt="" className="how_it_work_num_img" src={itemImg1Num1}/>
        </div>
        <Typography className="how_it_work_typography">
          <Title className="how_it_work_title">{t("chooseTariffTitle")}</Title>
          <Paragraph>
            {t("chooseTariffText")}
          </Paragraph>
        </Typography>

      </div>

      <div className="how_it_work_item">
        <div className="how_it_work_img_container">
          <img alt="" className="how_it_work_img" src={itemImg2}/>
          <img alt="" className="how_it_work_num_img" src={itemImg1Num2}/>
        </div>
        <Typography className="how_it_work_typography">
          <Title className="how_it_work_title">{t("writeToUsTitle")}</Title>
          <Paragraph>
            Напиши нам на <a target="_blank" href="mailto:modern.strategies.io@gmail.com" className="link">почту</a>,
            в <a target="_blank" href="https://wa.me/79017339775" className="link">WhatsApp</a> или <a
            target="_blank" href="https://t.me/modern_strategies" className="link">Telegram</a>,
            указанные в самом низу этой страницы, мы поможем на всех
            этапах, а также ответим на любые вопросы!
          </Paragraph>
        </Typography>

      </div>
      <div className="how_it_work_item">
        <div className="how_it_work_img_container">
          <img alt="" className="how_it_work_img" src={itemImg3}/>
          <img alt="" className="how_it_work_num_img" src={itemImg1Num3}/>
        </div>
        <Typography className="how_it_work_typography">
          <Title className="how_it_work_title">{t("makeDepositTitle")}</Title>
          <Paragraph>
            {t("makeDepositText")}
          </Paragraph>
        </Typography>

      </div>
      <div className="how_it_work_item">
        <div className="how_it_work_img_container">
          <img alt="" className="how_it_work_img" src={itemImg4}/>
          <img alt="" className="how_it_work_num_img" src={itemImg1Num4}/>
        </div>
        <Typography className="how_it_work_typography">
          <Title className="how_it_work_title">Зарабатывай и наблюдай </Title>
          <Paragraph>
            После вклада ты всегда сможешь увидеть информацию о своих вкладах и доходах
            в <a target="_blank" href="/ProfilePage" className="link">Личном кабинете</a>, а также сумму
            средств на балансах кошельков фонда, так как блокчейн подразумевает полную публичность данных. Эта ссылка поможет тебе отслеживать средства и заработок фонда: <a
            target="_blank" href="https://apeboard.finance/dashboard/0xD9eA96835cC3e6d93B1E9D572F265f1b10ee9EeC"
            style={{ color: '#13c2c2' }}>общий продвинутый кошелек</a>

          </Paragraph>
        </Typography>

      </div>

    </Element>
  )
}

export default HowItWork
