import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { api } from '../../../../../Store/api'

export const leedFormSend = createAsyncThunk(
    'form/leedFormSend',
    async (data) => {
        try {
            const response = await api.post('/user/request', {
                name: data.formValues.name,
                fieldName: data.formValues.fieldName,
                inputString: data.formValues.inputString
            });

            return response.data;
        } catch (e) {
            throw (e)
        }
    },
);
const leedFormSlice = createSlice({
    name: 'requests',
    initialState: {
        loading: false,
        adding: false,
        form: [],
        error: {
            failed: false,
            message: null,
        },
    },
    extraReducers: {
        [leedFormSend.pending]: (state) => {
            state.adding = true;
            state.error.failed = false;
            state.error.message = null;
        },
        [leedFormSend.fulfilled]: (state, action) => {
            state.adding = false;
            state.form.push({
                name: action.meta.arg.name,
                fieldName: action.meta.arg.fieldName,
                inputString: action.meta.arg.inputString,
            });
        },
        [leedFormSend.rejected]: (state, action) => {
            state.error.message = action.payload;
            state.error.failed = true;
        },
    }
});

export default leedFormSlice.reducer;


