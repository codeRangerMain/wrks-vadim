import React, { useState } from 'react'
import { leedFormSend } from './leedFormSlice'
import { message } from "antd";
import { Form, Input, Button, Select } from 'antd'
import { useDispatch, useSelector } from 'react-redux'
import { Element } from 'react-scroll'
const { Option } = Select;

const LeedForm = ({ t }) => {
  const [form] = Form.useForm()
  const dispatch = useDispatch()
  const adding = useSelector(state => state.leedFormSlice.adding)
  const onReset = () => {
    form.resetFields()
  }

  const success = () => {
    message.success({
      content: 'Заявка успешно отправлена, скоро наш менеджер свяжется с вами. Либо напишите нам, контакты есть внизу страницы',
      // className: 'custom-class',
      style: {
        marginTop: '17vh'
      }
    })
  }

  const sendData = (formValues) => {
    dispatch(
      leedFormSend({
        formValues
      }))
      .then(() => success())
      .then(() => onReset())


  }

  return (
    <Element name='leedForm' style={{ marginTop: '50px' }} className="leed">
      <div className="secttion_title">{t("leaveRequestTitle")}</div>
      <div className="leed-container">
        <div className="leed-text">
          {t("leaveRequestText")}
        </div>
        <div className="leed-form">

          <Form
            onFinish={sendData}
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 24,
            }}
            form={form}
            size="large"
            name="Оставить заявку"

            initialValues={{}}
            scrollToFirstError
          >
            <Form.Item
              name="name"
              label={t("formName")}
              rules={[
                {
                  required: true,
                  message: `${t("nameWarning")}`,
                  whitespace: true,
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              name="fieldName"
              label={t("communicationMethod")}
              rules={[
                {
                  required: true,
                  message: `${t("communicationWarning")}`,
                },
              ]}
            >
              <Select on placeholder={t("communicationMethodPlaceholder")}>
                <Option value="whatsapp">WhatsApp</Option>
                <Option value="telegram">Telegram</Option>
                <Option value="email">E-mail</Option>
                <Option value="viber">Viber</Option>
              </Select>
            </Form.Item>

            <Form.Item
              name="inputString"
              label={t("EnterData")}
              rules={[
                {
                  required: true,
                  message: `${t("enterDataWarning")}`,
                },
              ]}
            >
              <Input
                style={{
                  width: '100%',
                }}
              />
            </Form.Item>




            <Form.Item style={{ right: '0' }}>
              <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
                <Button style={{ alignSelf: 'flex-end' }} type="primary" htmlType="submit" disabled={adding}>
                  {t("leaveRequestButton")}
                </Button>
              </div>
            </Form.Item>
          </Form>
        </div>
      </div>
    </Element>
  )
}
export default LeedForm