import { Typography, Divider, Button } from 'antd'
import risksNum1 from '../../../../img/risksNum1.svg'
import risksNum2 from '../../../../img/risksNum2.svg'
import { Element } from 'react-scroll'
const { Title, Paragraph, Text, Link } = Typography;


function Risks() {
    return (

        <Element name='risks' style={{ marginTop: '50px' }} className='risks'>
            <div className="secttion_title">Риски</div>
            <div className="risks_item">
                <div className="risks_title">Возможный риск один:</div>
                <div className='risk_item'>
                    <img src={risksNum1} />
                    <Typography style={{ textAlign: 'center' }}>
                        <Title style={{ fontSize: '18px', lineHeight: '1' }}>Взлом контракта</Title>
                        <Paragraph style={{ lineHeight: '1.3' }}>
                            Из возможных рисков присутствует только один - злоумышленники, преследуя свои цели, могут взломать один из контрактов проекта, в который фонд инвестировал средства

                            DeFi сектор научился успешно бороться с такими ситуациями. Взломы случаются не так часто благодаря высокому качеству кода, который проходит множественные аудиты, а когда и случаются, то крупные доверенные проекты, в которые мы вкладываем, с вероятностью 99% организуют грамотную программу возврата средств, то есть присутствует риск только во временной задержке средств
                        </Paragraph>
                    </Typography>
                </div>

            </div>

        </Element>
    );
}

export default Risks;
