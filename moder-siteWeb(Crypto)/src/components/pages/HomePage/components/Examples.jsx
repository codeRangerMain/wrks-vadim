import { Carousel, } from 'antd'
import arrowLeft from '../../../../img/coruselArrowLeft.svg'
import arrowRight from '../../../../img/coruselArrowRight.svg'
import { Element } from 'react-scroll'
function Examples({ t }) {
    return (


        <Element name='examples' style={{ marginTop: '50px' }} className='examples'>
            <div className="secttion_title">{t("examples")}</div>

            <Carousel arrows
                prevArrow={<img alt='' src={arrowLeft} />}
                nextArrow={<img alt='' src={arrowRight} />}
                dots={true}
                className='carusel'    >
                <div>
                    <div className='examples_item'>
                        <div className='example_content'>
                            <div className='example_title'>{t("exampleOneTitle")}</div>
                            <div className='example_item'><div className='circle'></div>
                                {t("exampleOneTextP1")}
                            </div>
                            <div className='example_item'><div className='circle'></div>
                                {t("exampleOneTextP2")}
                            </div>
                            <div className='example_item'><div className='circle'></div>
                                {t("exampleOneTextP3")}
                            </div>
                            <div className='example_item'><div className='circle'></div>
                                {t("exampleOneTextP4")}

                            </div>
                        </div>

                    </div>

                </div>
                <div>
                    <div className='examples_item'>
                        <div className='example_content'>
                            <div className='example_title'>{t("exampleTwoTitle")}</div>
                            <div className='example_item'><div className='circle'></div>
                                {t("exampleTwoTextP1")}
                            </div>
                            <div className='example_item'><div className='circle'></div>
                                {t("exampleTwoTextP2")}
                            </div>
                            <div className='example_item'><div className='circle'></div>
                                {t("exampleTwoTextP3")}
                            </div>
                            <div className='example_item'><div className='circle'></div>
                                {t("exampleTwoTextP4")}
                            </div>
                        </div>
                    </div>
                </div>
                {/*<div>*/}
                {/*    <div className='examples_item'>*/}
                {/*        <div className='example_content'>*/}
                {/*            <div className='example_title'>Пример 3, Профессиональный:</div>*/}
                {/*            <div className='example_item'><div className='circle'></div> У вкладчика есть 150 000 рублей, он с нашей помощью покупает 2001 доллар, далее мы выводим его деньги на общий кошелек с комиссией 1$, инвестируем его деньги с комиссией 0.5%.*/}
                {/*            </div>*/}
                {/*            <div className='example_item'><div className='circle'></div> Итого сумма вклада 1990$. Планируемый годовой доход 200% или 3980$, или же 332$ в месяц. Хороший вариант, все комиссии отобьются за 1 день и потом пойдет прибыль, но нужно также учитывать возможные торговые риски, прибыль может быть ниже*/}
                {/*            </div>*/}
                {/*            <div className='example_item'><div className='circle'></div> За полгода получит 3980$, в фиате сможет получить 297 943 рублей (если курс доллара не изменится), или же около 98.6% дохода*/}
                {/*            </div>*/}
                {/*            <div className='example_item'><div className='circle'></div> За год получит 5970$, в фиате сможет получить 446 915 рублей (если курс доллара не изменится), или же около 197.9% дохода*/}
                {/*            </div>*/}
                {/*        </div></div>*/}
                {/*</div>*/}
                <div>
                    <div className='examples_item'>
                        <div className='example_content'>
                            <div className='example_title'>{t("exampleThreeTitle")}</div>
                            <div className='example_item'><div className='circle'></div>
                                {t("exampleThreeTextP1")}
                            </div>
                            <div className='example_item'><div className='circle'></div>
                                {t("exampleThreeTextP2")}
                            </div>
                            <div className='example_item'><div className='circle'></div>
                                {t("exampleThreeTextP3")}
                            </div>
                            <div className='example_item'><div className='circle'></div>
                                {t("exampleThreeTextP4")}
                            </div>
                        </div></div>
                </div>
            </Carousel>,

        </Element>
    );
}

export default Examples;