import React from 'react'
import { useAuth0 } from '@auth0/auth0-react'


function LogOut(props) {
  const { logout } = useAuth0()

  const logoutWithRedirect = () =>
    logout({
      returnTo: window.location.origin,
    });

  return (
    <div>

      <button
        onClick={() => logoutWithRedirect()}
      >
        Log out
      </button>
    </div>
  )
}

export default LogOut