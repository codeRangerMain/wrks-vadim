import { NavLink } from "react-router-dom";
import sadFace from '../../../img/sadFace.svg'
import Header from '../../Header'
function NotFoundPage() {


    return (
        <div className="not_found">
            <Header />
            <div className="not_found_container">
                <div className="content_wrapper">
                    <div className="not_found-content">
                        <div className="not_found-404">404 <img alt='упс' style={{ marginLeft: '10px', width: '80px', height: '80px' }} src={sadFace} /></div>
                        <div className='not_found-text'>
                            <h2 className='not_found-title'>Мы не можем найти эту страницу!</h2>
                            <p>Мы сожалеем, но страница на которую Вы пытались перейти не существует.<br />Пожалуйста вернитесь на предыдущую страницу или воспользуйтесь меню сайта</p>
                            <NavLink className="link link-404" to="/">На главную</NavLink>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    )
}

export default NotFoundPage