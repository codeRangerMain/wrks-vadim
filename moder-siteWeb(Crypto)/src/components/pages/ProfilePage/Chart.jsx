import { Area } from '@ant-design/charts'
import { setTime } from '../../../_presets';

function Chart(props) {

  let data = [];
  let setData = () => {
    if (props.data.investEarnings !== undefined) {
      props.data.investEarnings.map((current) => {
        data.push({ year: setTime(current.timestamp), value: current.amount, category: 'Доходность от депозита' });
      })
    }
    if (props.data.referralEarnings !== undefined) {
      props.data.referralEarnings.map((current) => {
        data.push({ year: setTime(current.timestamp), value: current.amount, category: 'Доходность от рефералов' });
      })
    }

  }

  if (props.data !== undefined) {
    setData()
  }
  const config = {
    data: data,
    grid: null,
    xField: 'year',
    yField: 'value',
    color: ['#13c2c2', '#0BD99E'],
    seriesField: 'category',
    xAxis: {
      fontSize: 100
    },
    point: {
      size: 4,
      shape: 'circle',
    },

    legend: { position: 'bottom' },
  };
  return <Area style={{ width: '100%' }} {...config} />;



}

export default Chart
