import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { api } from '../../../Store/api'
import { leedFormSend } from '../HomePage/components/LeedForm/leedFormSlice'
import { message } from 'antd'

export const fetchProfile = createAsyncThunk(
  'profile/fetchProfile',
  async ({ token }) => {
    api.interceptors.request.use(
      function (config) {
        config.headers.Authorization = token ? `Bearer ${token}` : ''
        if (localStorage.getItem('refLink') !== null) {
          config.headers.refId = localStorage.getItem('refLink').slice(1)
          localStorage.removeItem('refLink')
        }
        return config
      })
    const response = await api.get(`/i/user/info`)
    console.log(response)
    return response.data
  }
)

export const withDrawalForm = createAsyncThunk(
  'profile/withDrawalForm',
  async (data) => {
    try {
      const response = await api.post('/i/user/withdrawal', {
        packageId: data.packageId,
        usdAmount: data.usdAmount,
        requisites: data.requisites,
        status: false,
        contactWay: data.contactWay,
        contactDetails: data.contactDetails
      })
      return response.data
    } catch (e) {
      throw e
    }
  }
)

const profileSlice = createSlice({

  name: 'profile',
  initialState: {
    profileData: {},
    withDrawalData: [],
    newStatus: '',
    loading: false,
    adding: false,
    error: {
      failed: false,
      message: null
    },
    drawalError: {
      failed: '',
      message: null
    },
  },
  extraReducers: {
    //получение данных из профиля
    [fetchProfile.pending]: (state) => {
      state.loading = true
      state.error.failed = false
    },
    [fetchProfile.fulfilled]: (state, action) => {
      state.loading = false
      state.profileData = action.payload
      state.status = action.payload.status
    },
    [fetchProfile.rejected]: (state, action) => {
      state.loading = false
      state.error.failed = true
      state.error.message = action.payload
    },
    [withDrawalForm.pending]: (state) => {
      state.adding = true;
    },
    [withDrawalForm.fulfilled]: (state, action) => {
      state.adding = false;
      state.drawalError.failed = false
      state.withDrawalData.push({
        packageId: action.meta.arg.packageId,
        usdAmount: action.meta.arg.usdAmount,
        status: false,
        contactWay: action.meta.arg.contactWay,
        requisites: action.meta.arg.requisites,
        contactDetails: action.meta.arg.contactDetails,
      });
    },
    [withDrawalForm.rejected]: (state, action) => {
      state.drawalError.message = action.payload;
      state.drawalError.failed = true;
    },
  }

})

export default profileSlice.reducer