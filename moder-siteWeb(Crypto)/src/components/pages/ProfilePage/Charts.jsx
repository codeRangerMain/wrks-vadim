
import { Carousel } from 'antd'
import arrowLeft from '../../../img/coruselArrowLeft.svg'
import arrowRight from '../../../img/coruselArrowRight.svg'
import Chart from './Chart'
import { Area } from '@ant-design/charts'
function Charts(props) {

    if (props.earnings !== undefined && props.earnings.length > 0) {
        let charts = props.earnings.map((currentChart) => {
            function getTarrifInfo() {
                if (currentChart.packageName.toLowerCase() == 'beginner') {
                    return {
                        name: 'Начинающий',
                    }
                }
                if (currentChart.packageName.toLowerCase() == 'professional') {
                    return {
                        name: 'Профессиональный',
                    }
                }
                if (currentChart.packageName.toLowerCase() == 'advanced') {
                    return {
                        name: 'Продвинутый',
                    }

                }
                if (currentChart.packageName.toLowerCase() == 'large') {
                    return {
                        name: 'Крупный',
                    }
                }
                if (currentChart.packageName.toLowerCase() == 'creators') {
                    return {
                        name: 'Создатели',
                    }
                }
                if (currentChart.packageName.toLowerCase() == 'creators professional') {
                    return {
                        name: 'Создатели ПРО',
                    }
                }
                if (currentChart.packageName.toLowerCase() == 'for ours 1') {
                    return {
                        name: 'Для своих 1',
                    }

                }
                if (currentChart.packageName.toLowerCase() == 'for ours 2') {
                    return {
                        name: 'Для своих 2',
                    }
                }
                if (currentChart.packageName.toLowerCase() == 'for ours 3') {
                    return {
                        name: 'Для своих 3',
                    }
                }
                if (currentChart.packageName.toLowerCase() == 'for ours 4') {
                    return {
                        name: 'Для своих 4',
                    }
                }
                if (currentChart.packageName.toLowerCase() == 'for ours 5') {
                    return {
                        name: 'Для своих 5',
                    }
                }

                else {
                    return (currentChart.packageName)
                }
            }
            return (
                <div>
                    <div style={{ position: 'relative' }} className='examples_item'>
                        <div style={{ left: '30px', fontWeight: 'bold', fontSize: '24px' }}>{getTarrifInfo().name}</div>
                        <Chart data={currentChart} />
                    </div>
                </div >
            )
        })


        return (
            <Carousel arrows
                prevArrow={<img src={arrowLeft} />}
                nextArrow={<img src={arrowRight} />}
                dots={true}
                className='carusel carusel-chart'>
                {charts}
            </Carousel>
        )


    }
    else {
        return <Carousel arrows
            prevArrow={<img src={arrowLeft} />}
            nextArrow={<img src={arrowRight} />}
            dots={true}
            className='carusel carusel-chart'>
            <div>
                <div style={{ position: 'relative' }} className='examples_item'>
                    <div style={{ left: '30px', fontWeight: 'bold', fontSize: '24px' }}>График доходности</div>
                    {(function () {
                        const config = {
                            data: [{ year: '0', value: 0, category: 'Прибыль' },],
                            grid: null,
                            xField: 'year',
                            yField: 'value',
                            color: ['#13c2c2', '#0BD99E'],
                            seriesField: 'category',
                            xAxis: {
                                fontSize: 100
                            },
                            point: {
                                size: 4,
                                shape: 'circle',
                            },

                            legend: { position: 'bottom' },
                        };
                        return <Area style={{ width: '100%' }} {...config} />;
                    }())
                    }
                </div>
            </div>
        </Carousel>
    }
}

export default Charts