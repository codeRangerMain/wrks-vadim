import arrow from '../../../img/arrowRight.svg'
import { Modal } from 'antd';
import { useState } from 'react';
import telegramLogo from '../../../img/telegramLogo.svg'
import whatAppLogo from '../../../img/whatAppIco.svg'


function ActivePackage(props) {
    const [isModalVisible, setIsModalVisible] = useState(false);
    const showModal = (e) => {
        setIsModalVisible(true);
    };

    const handleOk = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    function getSliceBalance() {
        return (
            props.package.balance.toString().split('.')[0]
        )
    }
    function getTarrifInfo() {
        if (props.package.packageName.toLowerCase() === 'beginner') {
            return {
                name: 'Начинающий',
                procent: '15',
                wallet: 'Общий',
                enlightenment: 'Каждый день'
            }
        }
        if (props.package.packageName.toLowerCase() === 'professional') {
            return {
                name: 'Профессиональный',
                procent: 'до 200',
                wallet: 'Общий',
                enlightenment: 'Каждый день'
            }
        }
        if (props.package.packageName.toLowerCase() === 'advanced') {
            return {
                name: 'Продвинутый',
                procent: '19',
                wallet: 'Общий',
                enlightenment: 'Раз в неделю'
            }

        }
        if (props.package.packageName.toLowerCase() === 'large') {
            return {
                name: 'Крупный',
                procent: '20',
                wallet: 'Общий или личный',
                enlightenment: 'Раз в неделю'
            }
        }
        if (props.package.packageName.toLowerCase() === 'creators') {
            return {
                name: 'Создатели',
                procent: 'все',
                wallet: 'Общий',
                enlightenment: 'Раз в неделю'
            }
        }
        if (props.package.packageName.toLowerCase() === 'creators professional') {
            return {
                name: 'Создатели ПРО',
                procent: 'все',
                wallet: 'Общий',
                enlightenment: 'Каждый день'
            }
        }
        if (props.package.packageName.toLowerCase() === 'for ours 1') {
            return {
                name: 'Для своих 1',
                procent: '22',
                wallet: 'Общий',
                enlightenment: 'Раз в неделю'
            }

        }
        if (props.package.packageName.toLowerCase() === 'for ours 2') {
            return {
                name: 'Для своих 2',
                procent: '24',
                wallet: 'Общий',
                enlightenment: 'Раз в неделю'
            }
        }
        if (props.package.packageName.toLowerCase() === 'for ours 3') {
            return {
                name: 'Для своих 3',
                procent: '26',
                wallet: 'Общий',
                enlightenment: 'Раз в неделю'
            }
        }
        if (props.package.packageName.toLowerCase() === 'for ours 4') {
            return {
                name: 'Для своих 4',
                procent: '22',
                wallet: 'Общий',
                enlightenment: 'Раз в неделю'
            }
        }
        if (props.package.packageName.toLowerCase() === 'for ours 5') {
            return {
                name: 'Для своих 5',
                procent: '30',
                wallet: 'Общий',
                enlightenment: 'Раз в неделю'
            }
        }

        else {
            return (props.package.packageName)
        }
    }
    return (
        <div style={{ color: '#000' }} className='tairffs_item'>
            <div style={{ color: '#000' }} className='tairffs_item_title'>{getTarrifInfo().name}</div>
            <div className='tairffs_item_price'><span style={{ position: 'relative', top: '10px' }}></span> <span style={{ marginLeft: '10px', marginRight: '10px' }} className="price_bold">{getSliceBalance()}</span> <span style={{ position: 'relative', fontSize: '34px', fontWeight: 'bold' }}>$</span></div>
            <div style={{ color: '#fff' }} className='tairffs_about'>
                <div style={{ color: 'black' }} className="tairffs_about_item">  <div><img alt="" className='tairffs_item_arrow' src={arrow} />Доходность в год </div> <span style={{ fontWeight: 'bold', fontSize: '20px' }}>{getTarrifInfo().procent}%</span></div>
                <div style={{ marginBottom: '5px', color: 'black' }} className="tairffs_about_item"> <div><img alt="" className='tairffs_item_arrow' src={arrow} />Тип кошелька</div>  <span>{getTarrifInfo().wallet}</span></div>
                <div className="tairffs_about_item" style={{ color: 'black' }}> <div><img alt="" className='tairffs_item_arrow' src={arrow} />Начисления</div>  <span>{getTarrifInfo().enlightenment}</span></div>
            </div>
            <Modal cancelText="Закрыть" title="Контакты" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
                <div className="footer_social">
                    <div style={{ color: 'rgb(133, 143, 149)', marginBottom: '13px' }}>По всем вопросам обращайтесь:
                    </div>
                    <div className="footer_social_item">
                        <img alt='' className="footer_telegram_logo" src={telegramLogo} /><a target="_blank" href='https://t.me/modern_strategies' style={{ color: '#35A2D9' }}>Мы<span style={{ color: '#000' }}> в Telegram </span></a>
                    </div>
                    <div className="footer_social_item">
                        <img alt='' style={{ width: '25px', height: '25px' }} className="footer_telegram_logo" src={whatAppLogo} /><a target="_blank" href='https://wa.me/79017339775' style={{ color: '#35A2D9' }}>Мы <span style={{ color: '#000' }}>в WhatsApp</span></a>
                    </div>
                    <div style={{ color: '#858F95' }} className="footer_social_item">
                        <img alt="" style={{ marginLeft: '3px' }} className="footer_telegram_logo" /><a target="_blank" style={{ color: 'rgb(133, 143, 149)' }} href="mailto:modern.strategies.io@gmail.com">modern.strategies.io@gmail.com</a>
                    </div>
                </div>
            </Modal>
            <a onClick={showModal} style={{ width: '170px', alignSelf: 'center' }} className='btn'>Пополнить</a>
        </div>
    )
}

function ActivePackages(props) {

    if (props.activePackages !== undefined && props.activePackages.length > 0) {
        let packages = props.activePackages.map((currentPackage) => {
            return <ActivePackage package={currentPackage} />
        })

        return (
            <div className="tairffs_container">
                {packages}
            </div>
        )
    }
    else { return <div style={{ fontWeight: 'bold', color: 'grey', textAlign: 'center' }}> Нет активных пакетов</div> }
}


export default ActivePackages