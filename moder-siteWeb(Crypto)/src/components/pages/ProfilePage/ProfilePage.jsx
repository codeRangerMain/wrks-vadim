import '../../../App.css'
import Header from '../../Header'
import NavHeader from '../../NavHeader'
import Footer from '../../Footer'
import LastTransactions from './LastTransactions'
import PersonalEarningInfo from './PersonalEarningInfo'
import ActivePackages from './ActivePackages'
import Charts from './Charts'
import { useDispatch, useSelector } from 'react-redux'
import { useAuth0, withAuthenticationRequired } from '@auth0/auth0-react'
import { fetchProfile } from './profileSlice'
import { useEffect } from 'react'
import Loading from '../../Loading'





function ProfilePage() {
    const { getAccessTokenSilently } = useAuth0()

    useSelector(state => state.profileSlice.profileStatusData)
    const response = useSelector(state => state.profileSlice.profileData)
    console.log(response)
    let getPersonalBalance = () => {
        if (response.data !== undefined) {
            return {
                summaryDeposits: response.data.summaryDeposits,
                activePackagesCount: response.data.activePackagesCount,
                summaryEarning: response.data.summaryEarning,
            }
        }
        else { return undefined }
    }

    const dispatch = useDispatch()

    useEffect(() => {
        getAccessTokenSilently().then(token => {
            dispatch(fetchProfile({ token }))
        })

    }, [dispatch, getAccessTokenSilently]);

    return (
        <div className="profile-page">
            <div style={{ marginBottom: '50px', height: '200px' }} className="page_header_bg">
                <Header />
                <NavHeader name="Личный кабинет" path="/" toPage="На главную" />
            </div>
            <div className="content_wrapper">
                {response.data !== undefined ?
                    <div>
                        <div className='chart-container'>
                            <Charts earnings={response.data.earnings} />
                            <PersonalEarningInfo refLink={response.data.refLink} balance={getPersonalBalance()} />
                        </div>
                        <section style={{ marginTop: '80px' }} className="active-packages">
                            <div className="secttion_title">Активные пакеты</div>
                            <ActivePackages activePackages={response.data.balances} />
                        </section>
                        <LastTransactions lastEvents={response.data.lastEvents} />

                    </div> : <Loading />}
            </div>
            <Footer />
        </div>

    )
}

export default withAuthenticationRequired(ProfilePage, {
    onRedirecting: () => '/',
})







// const lastEvents = [
    //     {
    //         type: "earning_investment",
    //         info: "Инвестиционная прибыль -3.37937643$ по тарифу Creators professional",
    //         balanceChange: -3.37937643,
    //         timestamp: 1625485506000
    //     },
    //     {
    //         type: "earning_investment",
    //         info: "Инвестиционная прибыль 4.34491255$ по тарифу Creators professional",
    //         balanceChange: 4.34491255,
    //         timestamp: 1625409394000
    //     },
    //     {
    //         type: "earning_investment",
    //         info: "Инвестиционная прибыль 7.96567301$ по тарифу Creators professional",
    //         balanceChange: 7.96567301,
    //         timestamp: 1625319549000
    //     },
    //     {
    //         type: "earning_investment",
    //         info: "Инвестиционная прибыль -17.36359852$ по тарифу Creators",
    //         balanceChange: -17.36359852,
    //         timestamp: 1625319476000
    //     },
    //     {
    //         type: "earning_investment",
    //         info: "Инвестиционная прибыль -4.90009582$ по тарифу Creators professional",
    //         balanceChange: -4.90009582,
    //         timestamp: 1625220920000
    //     },
    //     {
    //         type: "earning_investment",
    //         info: "Инвестиционная прибыль -10.78986616$ по тарифу Creators professional",
    //         balanceChange: -10.78986616,
    //         timestamp: 1625143881000
    //     },
    //     {
    //         type: "earning_investment",
    //         info: "Инвестиционная прибыль -5.31044867$ по тарифу Creators professional",
    //         balanceChange: -5.31044867,
    //         timestamp: 1625055340000
    //     },
    //     {
    //         type: "earning_investment",
    //         info: "Инвестиционная прибыль 5.79321673$ по тарифу Creators professional",
    //         balanceChange: 5.79321673,
    //         timestamp: 1624964527000
    //     },
    //     {
    //         type: "earning_investment",
    //         info: "Инвестиционная прибыль -67.34614451$ по тарифу Creators professional",
    //         balanceChange: -67.34614451,
    //         timestamp: 1624876300000
    //     },
    //     {
    //         type: "earning_investment",
    //         info: "Инвестиционная прибыль 10.86228137$ по тарифу Creators professional",
    //         balanceChange: 10.86228137,
    //         timestamp: 1624798943000
    //     }
    // ]
    // const personalBalance = {
    //     summaryDeposits: 8725.1425409029,
    //     activePackagesCount: 2,
    //     summaryEarning: -135.64177347,
    // }
    // let earnings = [
    //     {
    //         packageName: "Creators",
    //         investEarnings: [
    //             {
    //                 amount: 0,
    //                 packageName: "Creators",
    //                 timestamp: 1624659685000
    //             },
    //             {
    //                 amount: -17.36359852,
    //                 packageName: "Creators",
    //                 timestamp: 1625319476000
    //             }
    //         ],
    //         referralEarnings: []
    //     },
    //     {
    //         packageName: "Creators professional",
    //         investEarnings: [
    //             {
    //                 amount: -55.51832702,
    //                 packageName: "Creators professional",
    //                 timestamp: 1624709855000
    //             },
    //             {
    //                 amount: 10.86228137,
    //                 packageName: "Creators professional",
    //                 timestamp: 1624798943000
    //             },
    //             {
    //                 amount: -67.34614451,
    //                 packageName: "Creators professional",
    //                 timestamp: 1624876300000
    //             },
    //             {
    //                 amount: 5.79321673,
    //                 packageName: "Creators professional",
    //                 timestamp: 1624964527000
    //             },
    //             {
    //                 amount: -5.31044867,
    //                 packageName: "Creators professional",
    //                 timestamp: 1625055340000
    //             },
    //             {
    //                 amount: -10.78986616,
    //                 packageName: "Creators professional",
    //                 timestamp: 1625143881000
    //             },
    //             {
    //                 amount: -4.90009582,
    //                 packageName: "Creators professional",
    //                 timestamp: 1625220920000
    //             },
    //             {
    //                 amount: 7.96567301,
    //                 packageName: "Creators professional",
    //                 timestamp: 1625319549000
    //             },
    //             {
    //                 amount: 4.34491255,
    //                 packageName: "Creators professional",
    //                 timestamp: 1625409394000
    //             },
    //             {
    //                 amount: -3.37937643,
    //                 packageName: "Creators professional",
    //                 timestamp: 1625485506000
    //             }
    //         ],
    //         referralEarnings: []
    //     }
    // ]

    // let activeTariffs = [
    //     {
    //         packageId: 10,
    //         packageName: "Creators",
    //         apr: 1000000,
    //         balance: 8618.9335674829
    //     },
    //     {
    //         packageId: 11,
    //         packageName: "Creators professional",
    //         apr: 0,
    //         balance: 106.20897342
    //     }
    // ]