import { Modal } from 'antd';
import telegramLogo from '../../../img/telegramLogo.svg'
import whatAppLogo from '../../../img/whatAppIco.svg'

import React, { useEffect, useState } from 'react'
import { withDrawalForm } from './profileSlice';
import { message } from "antd";
import { Form, Input, Button, Select, Tooltip } from 'antd'
import { useDispatch, useSelector } from 'react-redux'
import { Element } from 'react-scroll'
import { useParams } from 'react-router-dom';
const { Option } = Select;
let usdAmount = ''
let requisites = ''
let packageId = ''
let contactWay = ''
let contactDetails = ''
function PersonalEarningInfo(props) {

    const packages = useSelector((state) => {
        let stateCopy = JSON.parse(JSON.stringify(state.profileSlice.profileData.data))


        stateCopy.balances.map((currentPackage) => {

            if (currentPackage.packageName.toLowerCase() === 'beginner') {
                currentPackage.packageName = 'Начинающий'
            }
            if (currentPackage.packageName.toLowerCase() === 'professional') {
                currentPackage.packageName = 'Профессиональный'
            }
            if (currentPackage.packageName.toLowerCase() === 'advanced') {
                currentPackage.packageName = 'Продвинутый'
            }
            if (currentPackage.packageName.toLowerCase() === 'large') {
                currentPackage.packageName = 'Крупный'
            }
            if (currentPackage.packageName.toLowerCase() === 'creators') {
                currentPackage.packageName = 'Создатели'
            }
            if (currentPackage.packageName.toLowerCase() === 'creators professional') {
                currentPackage.packageName = 'Создатели ПРО'
            }
            if (currentPackage.packageName.toLowerCase() === 'for ours 1') {
                currentPackage.packageName = 'Для своих 1'
            }
            if (currentPackage.packageName.toLowerCase() === 'for ours 2') {
                currentPackage.packageName = 'Для своих 2'
            }
            if (currentPackage.packageName.toLowerCase() === 'for ours 3') {
                currentPackage.packageName = 'Для своих 3'
            }
            if (currentPackage.packageName.toLowerCase() === 'for ours 4') {
                currentPackage.packageName = 'Для своих 4'
            }
            if (currentPackage.packageName.toLowerCase() === 'for ours 5') {
                currentPackage.packageName = 'Для своих 5'
            }
        })
        return stateCopy.balances
    })


    const error = useSelector(state => state.profileSlice.drawalError.failed)
    const [form] = Form.useForm()
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [modalContent, setModalContent] = useState('')
    const [modalTitle, setModalTitle] = useState('')
    const [okTitle, setOkTitle] = useState('')
    const [selcetPlaceHolderContent, setSelcetPlaceHolderContent] = useState('')
    const refLink = props.refLink
    const dispatch = useDispatch()
    const checkActivePackages = () => {
        if (packages.length > 0) {
            setSelcetPlaceHolderContent('Выберите тариф для снятия')
        }
        else {
            setSelcetPlaceHolderContent('У вас нет активных пакетов')
        }
    }
    useEffect(() => {
        checkActivePackages()
    }, [packages])

    let setUsdAmount = (e) => {
        usdAmount = e.target.value
    }
    let setRequisites = (e) => {
        requisites = e.target.value
    }
    let setPackageId = (e) => {
        packageId = e

    }
    let setContactWay = (e) => {
        contactWay = e

    }
    let setContactDetails = (e) => {
        contactDetails = e.target.value
    }

    const checkPackages = () => {
        if (packages.length > 0) {
            return (false)
        }
        else {
            return (true)
        }
    }
    const handleOk = (e) => {
        if (e.target.textContent === 'Вывести') {
            dispatch(
                withDrawalForm(
                    {
                        usdAmount,
                        requisites,
                        packageId,
                        contactWay,
                        contactDetails,
                    }
                ))
                .then(() => form.resetFields())
        }

        else if (e.target.textContent === 'Скопировать') {
            let refInput = document.getElementById('refInput')
            refInput.select();
            document.execCommand("copy");
            message.success({
                content: 'Ссылка удачно скопированна!',
                className: 'custom-class',
                style: {
                    marginTop: '17vh'
                }
            })

        }
        setIsModalVisible(false);

    };
    useEffect(() => {
        if (error && error !== '') {
            message.error({
                content: 'При попытке вывода средств произошла ошибка, попробуйте снова позже, либо свяжитесь с вашим менеджером',
                style: {
                    marginTop: '50vh'
                }
            })
        }
        else if (!error && error !== '') {
            message.success({
                content: 'Заявка успешно отправлена, скоро наш менеджер свяжется с вами.',
                className: 'custom-class',
                style: {
                    marginTop: '17vh'
                }
            })

        }

    }, [error])


    const showModal = (e) => {

        let target = e.target.id
        if (target === 'showContact') {
            setModalTitle('Контакты')
            setOkTitle('OK')
            setModalContent(<div className="footer_social">
                <div style={{ color: 'rgb(133, 143, 149)', marginBottom: '13px' }}>По всем вопросам обращайтесь:
                </div>
                <div className="footer_social_item">
                    <img alt='' className="footer_telegram_logo" src={telegramLogo} /><a target="_blank" href='https://t.me/modern_strategies' style={{ color: '#35A2D9' }}>Мы<span style={{ color: '#000' }}> в Telegram </span></a>
                </div>
                <div className="footer_social_item">
                    <img alt='' style={{ width: '25px', height: '25px' }} className="footer_telegram_logo" src={whatAppLogo} /><a target="_blank" href='https://wa.me/79017339775' style={{ color: '#35A2D9' }}>Мы <span style={{ color: '#000' }}>в WhatsApp</span></a>
                </div>
                <div style={{ color: '#858F95' }} className="footer_social_item">
                    <img alt="" style={{ marginLeft: '3px' }} className="footer_telegram_logo" /><a target="_blank" style={{ color: 'rgb(133, 143, 149)' }} href="mailto:modern.strategies.io@gmail.com">modern.strategies.io@gmail.com</a>
                </div>
            </div>)
        }
        else if (target === 'showForm') {
            setModalTitle('Вывод')
            setOkTitle('Вывести')
            setModalContent(
                <Element name='leedForm' style={{}} className="leed">
                    {/* <div className="secttion_title">Оставь заявку и получи бесплатную консультацию</div> */}
                    <div style={{ width: 'auto', border: 'none', padding: '0 0 0 0 ' }} className="leed-form">
                        <Form

                            labelCol={{
                                span: 8,
                            }}
                            wrapperCol={{
                                span: 24,
                            }}
                            form={form}
                            size="large"
                            name="Оставить заявку"

                            initialValues={{}}
                            scrollToFirstError
                        >
                            <Form.Item
                                name="setPackageId"
                                label="Выберите тариф"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Пожалуйста выберите один из вариантов!',
                                    },
                                ]}
                            >
                                <Select disabled={checkPackages()} onChange={setPackageId} placeholder={selcetPlaceHolderContent}>

                                    {packages.map((currentPackage) => {
                                        return <Option value={currentPackage.packageId}>{currentPackage.packageName}</Option>
                                    })}


                                </Select>
                            </Form.Item>
                            <Form.Item
                                name="setUsdAmount"
                                label="Введите сумму"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Пожалуйста введите сумму!',
                                    },
                                ]}
                            >
                                <Input onChange={setUsdAmount}
                                    style={{
                                        width: '100%',
                                    }}
                                />
                            </Form.Item>
                            <Form.Item
                                name="setRequisites"
                                label="Введите реквизиты"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Пожалуйста введите реквизиты!',

                                    },
                                ]}
                            >
                                <Input onChange={setRequisites}
                                    style={{
                                        width: '100%',
                                    }}
                                />
                            </Form.Item>

                            <Form.Item
                                name="setContactWay"
                                label="Удобный способ связи"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Пожалуйста выберите один из вариантов!',
                                    },
                                ]}
                            >
                                <Select onChange={setContactWay} on placeholder="Выберите удобный способ связи">
                                    <Option value="whatsapp">WhatsApp</Option>
                                    <Option value="telegram">Telegram</Option>
                                    <Option value="email">E-mail</Option>
                                    <Option value="viber">Viber</Option>
                                </Select>
                            </Form.Item>

                            <Form.Item
                                name="setContactDetails"
                                label="Введите контакты"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Пожалуйста введите контактную иформацию!',
                                    },
                                ]}
                            >
                                <Input onChange={setContactDetails}
                                    style={{
                                        width: '100%',
                                    }}
                                />
                            </Form.Item>
                        </Form>
                    </div>
                </Element>
            )
        }
        else if (target === 'showRefLink') {
            setModalTitle('Ваша реферальная ссылка')
            setOkTitle('Скопировать')

            setModalContent(
                <div>
                    <div>
                        <p>Заработок фонда состоит из комиссии от прибыли вкладчиков. Средний размер комиссии <b>20%</b></p>

                        <p>Вы будете получать <b>20%</b> от прибыли фонда с тех вкладчиков, которые придут по вашей ссылке</p>

                        <p>Например: ваш реферал сделал депозит в размере <b>10 000$</b>, и за какой-то срок заработал <b>1000$</b>. Ваш доход составит <b>200$</b></p>
                    </div>
                    <Input id='refInput' style={{ cursor: 'not-allowed' }} value={refLink} />
                </div>
            )
        }
        setIsModalVisible(true);

    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    if (props.balance !== undefined) {
        let getSummaryDeposits = () => {
            return (
                props.balance.summaryDeposits.toString().split('.')[0]
            )
        }
        let getSummaryEarning = () => {
            return (
                props.balance.summaryEarning.toString().split('.')[0]
            )
        }

        return (
            <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                <div className="profile-investment-dashboard">
                    <div className="profile-investment-dashboard-item">
                        <div>
                            Сумма активных инвестиций
                        </div>
                        <div style={{ fontSize: '30px', color: '#13c2c2' }} className='price_bold'>{getSummaryDeposits()}$</div>
                    </div>
                    <div className="profile-investment-dashboard-item">
                        <div>
                            Количество активных пакетов
                        </div>
                        <div style={{ fontSize: '30px', color: '#13c2c2' }} className='price_bold'>{props.balance.activePackagesCount}</div>
                    </div>
                    <div className="profile-investment-dashboard-item">
                        <div>
                            Получено прибыли по пакетам
                        </div>
                        <div style={{ fontSize: '30px', color: '#13c2c2' }} className='price_bold'>{getSummaryEarning()}$</div>
                    </div>
                    <Modal okText={okTitle} cancelText="Закрыть" title={modalTitle} visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
                        {modalContent}
                    </Modal>
                </div>
                <a id='showContact' onClick={showModal} className='btn btn-profile'>Связь с менеджером</a>
                <a id='showForm' style={{ marginTop: '10px' }} onClick={showModal} className='btn btn-profile'>Вывод средств</a>
                <a id='showRefLink' style={{ marginTop: '10px', borderColor: '#DEE3E6', backgroundColor: '#DEE3E6', color: '#758189' }} onClick={showModal} className='btn btn-profile btn-black'>Реферальная ссылка</a>
            </div>
        )
    }
    else { return "" }
}

export default PersonalEarningInfo
