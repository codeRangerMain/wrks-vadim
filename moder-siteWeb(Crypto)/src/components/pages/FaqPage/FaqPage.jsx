import React, { useState } from 'react';
import infoImg from '../../../img/information.svg'
import '../../../App.css'
import Header from '../../Header'
import NavHeader from '../../NavHeader'
import Footer from '../../Footer'
import { Modal } from 'antd';
function FaqPage() {
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [modalTitle, setModalTitle] = useState('');
    const [modalContent, setModalContent] = useState('');

    const showModal = (e) => {

        let faqNumber = e.target.textContent.split('.')[0]
        let faqTitle = e.target.textContent
        setModalTitle(faqTitle)


        if (faqNumber === '1') {
            setModalContent('В данный момент российский рубль нестабилен, больше доверия вызывают иностранные валюты, в частности, доллар и евро. Положив валюту в банк, в лучшем случае можно выручить 1.5% годовых (при этом срок на несколько лет, нельзя ни снять, ни добавить до конца срока, вклад от нескольких миллионов в рублей эквиваленте, не все могут себе позволить). Зачастую доходность банковских депозитов даже не покрывает инфляцию валюты. Инвесторы хотят зарабатывать хорошую доходность в долларе/евро и мы помогаем им решить эту задачу')
        }
        if (faqNumber === '2') {
            setModalContent('Фонд не может гарантировать прибыль по тарифам. Наши аналитики стараются соблюдать баланс прибыли и безопасности, а также всегда показывать фактическую прибыль близкую к предполагаемой')
        }
        if (faqNumber === '3') {
            setModalContent('Визуализация средств доступна по ссылке: <a class="link" href="https://apeboard.finance/dashboard/0xE6334EA4E13c8f364E8AA518907440b8A7BcDcb3">начинающий и продвинутый</a>')
        }
        if (faqNumber === '4') {
            setModalContent('Визуализатор может не учитывать некоторые инвестиции фонда, но мы держим с администрацией ape board прямой контакт и они рано или поздно добавляют отображение тех инвестиций, которые могут быть не видны')
        }
        if (faqNumber === '5') {
            setModalContent('<p>От английского, decentralized finance, или же, по-русски, децентрализованные финансы. Этим термином обозначаются все проекты в криптовалютной индустрии, которые управляются напрямую пользователями этих проектов.</p> <p>Например, биржа <a href="https://binance.com/" class="link">Binance</a>(крупнейшая крипто-биржа в мире), централизована, так как имеет свое собственное руководство и все решения принимаются только им. В то же время криптовалютный обменник <a href="https://app.uniswap.org/" class="link">Uniswap</a>(крупнейший DeFi обменник), полностью децентрализован и все решения принимаются пользовательским сообществом посредством голосований.</p> <p>Что это значит для нас? Простым языком, Binance, имеет возможность в любой момент заблокировать счет и обнулить средства любого пользователя, в то время как Uniswap не имеет такой возможности. Исходный код DeFi проектов открыт и публичен, любой желающий может его просмотреть и проанализировать, перейдя по этой <a class="link" href="https://etherscan.io/address/0x7a250d5630b4cf539739df2c5dacb4c659f2488d#code">ссылке</a></p>')
        }
        if (faqNumber === '6') {
            setModalContent('<p>Стандартных варианта три:</p><p>1) Комиссии с обменов. В стандартных централизованных биржах для поддержания актуального курса валют, например EUR-USD, нужно большое количество заявок на покупку/продажу, другими словами, ликвидность. Эту самую ликвидность создают пользователи, которые действительно хотят совершать какие-то операции на бирже, при этом не получая за это вознаграждение, а только оплачивая комиссию за сделки, но зачастую этого бывает недостаточно. В этот момент на поле выходят специальные компании, которые готовы предоставлять эту ликвидность за вознаграждение, и все биржи этим охотно пользуются. Такие компании называются market makers, они получают доход на свои вложенные средства в ликвидность.</p><p>Децентрализованным обменникам точно также необходима ликвидность в огромных размерах для поддержания актуального курса в валютных парах, и любой желающий может ее предоставить. Например в торговой паре ETH-USDT нужно предоставлять 50% в Эфириуме, и 50% в USDT, при этом появляется риск потери средств при падении курса Эфириума, что нам совершенно неинтересно. Наш фонд старается подбирать только те торговые пары, в которых отсутствует торговый риск, то есть пары типа USDT-BUSD, в которых оба актива являются криптодолларами, жестко привязанными к цене американского доллара USD</p><p>2) Займы. Да, в криптовалютной индустрии существуют займы, причем уже достаточно давно. Все займы всегда обеспечены каким-то активом, например Эфириумом или любой другой популярной криптовалютой. То есть у кредитора нет риска невозврата средств, так как все займы обеспечены залогом сверх суммы займа с коэффициентом 1.428</p><p>За 1000$ займа в USDT, стабильной криптовалюте, привязанной к доллару, заемщик должен предоставить 1428$ залога в BTC, ETH или другой волатильной криптовалюте. В крипто займах повышенный процент доходности</p><p>3) Стейкинги. Некоторым проектам нужны средства для каких-то своих целей, будь то увеличение показателей роста, распространение токенов проекта или что-то другое. Зачастую они платят большие деньги за предоставление им средств, но не в стабильных долларах, а в своих собственных монетах, которых они могут напечатать сколько угодно, но это на усмотрение сообщества. Эти монеты могут дорожать или обесцениваться, наша задача в том, чтобы вовремя переводить данные фантики в стабильные доллары, в которых нет риска волатильности. При этом наш вклад также безопасен, так как мы предоставляем проекту стабильные токены, привязанные к $</p>')
        }
        if (faqNumber === '7') {
            setModalContent('<p>Торговых рисков нет. Из возможных рисков присутствует только один - злоумышленники, преследуя свои цели, могут взломать один из контрактов проекта, в который фонд инвестировал средства</p>DeFi сектор научился успешно бороться с такими ситуациями. Взломы случаются не так часто благодаря высокому качеству кода, который проходит множественные аудиты, а когда и случаются, то крупные доверенные проекты, в которые мы вкладываем, с вероятностью 95% организуют грамотную программу возврата средств<p></p>')
        }
        if (faqNumber === '8') {
            setModalContent('<p>Прибыль начисляется каждую субботу</p>')
        }
        if (faqNumber === '9') {
            setModalContent('<p>Деньги можно вывести не раньше чем через 2 недели после депозита в любой выходной день</p>')
        }
        if (faqNumber === '10') {
            setModalContent('<p>Нет, наши финансовые аналитики сделают эту работа за вас, а фонд лишь взимает комиссию с прибыли</p>')
        }
        if (faqNumber === '11') {
            setModalContent('<p>Фонд взимает около 20% комиссии с доходов наших клиентов</p>')
        }
        setIsModalVisible(true);
    };

    const handleOk = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    return (
        <section className="faq_page">
            <div style={{ marginBottom: '50px', height: '200px' }} className="page_header_bg">
                <Header />
                <NavHeader name="FAQ" path="/" toPage="На главную" />
            </div>
            <div className='content_wrapper'>
                <h1 className="secttion_title">
                    Часто задаваемые вопросы
                </h1>
                <div className="faq" style={{ maxWidth: '760px', marginTop: '0px', border: '2px solid #13c2c2', borderRadius: '10px', padding: '20px', backgroundColor: '#fff' }}>
                    <div className="faq-container">
                        <div onClick={showModal} className='faq-item'><img style={{ width: '20px', height: '20px', marginRight: '10px' }} alt='' src={infoImg} /><div className="faq-text">1. Какую задачу решает фонд?</div>  </div>
                        <div onClick={showModal} className='faq-item'><img style={{ width: '20px', height: '20px', marginRight: '10px' }} alt='' src={infoImg} /><div className="faq-text">2. Может ли фонд гарантировать прибыль?</div>  </div>
                        <div onClick={showModal} className='faq-item'><img style={{ width: '20px', height: '20px', marginRight: '10px' }} alt='' src={infoImg} /><div className="faq-text">3. Где можно следить за средствами фонда?</div>  </div>
                        <div onClick={showModal} className='faq-item'><img style={{ width: '20px', height: '20px', marginRight: '10px' }} alt='' src={infoImg} /><div className="faq-text">4. Почему различаются данные в визуализации и на главной странице фонда?</div>  </div>
                        <div onClick={showModal} className='faq-item'><img style={{ width: '20px', height: '20px', marginRight: '10px' }} alt='' src={infoImg} /><div className="faq-text">5. Что такое <span style={{ color: '#13c2c2' }} > DeFi?</span></div>  </div>
                        <div onClick={showModal} className='faq-item'><img style={{ width: '20px', height: '20px', marginRight: '10px' }} alt='' src={infoImg} /><div className="faq-text">6. Как фонд зарабатывает деньги?</div>  </div>
                        <div onClick={showModal} className='faq-item'><img style={{ width: '20px', height: '20px', marginRight: '10px' }} alt='' src={infoImg} /><div className="faq-text">7. Есть ли торговые риски?</div>  </div>
                        <div onClick={showModal} className='faq-item'><img style={{ width: '20px', height: '20px', marginRight: '10px' }} alt='' src={infoImg} /><div className="faq-text">8. Как часто начисляется прибыль?</div>  </div>
                        <div onClick={showModal} className='faq-item'><img style={{ width: '20px', height: '20px', marginRight: '10px' }} alt='' src={infoImg} /><div className="faq-text">9. Когда можно выводить деньги?</div>  </div>
                        <div onClick={showModal} className='faq-item'><img style={{ width: '20px', height: '20px', marginRight: '10px' }} alt='' src={infoImg} /><div className="faq-text">10. Нужно ли мне разбираться в криптовалюте и технологиях чтобы заработать здесь?</div>  </div>
                        <div onClick={showModal} className='faq-item'><img style={{ width: '20px', height: '20px', marginRight: '10px' }} alt='' src={infoImg} /><div className="faq-text">11. Как зарабатывает фонд?</div>  </div>
                    </div>
                    <Modal title={modalTitle} visible={isModalVisible} onOk={handleOk} cancelText="Закрыть" onCancel={handleCancel}>
                        <div dangerouslySetInnerHTML={{ __html: modalContent }}></div>
                    </Modal>
                </div>
            </div>
            <Footer />

        </section>

    )
}

export default FaqPage
