import arrowUp from '../../../../img/upArrow.svg'
import arrowDown from '../../../../img/downArrow.svg'
import { setTransactionTime } from '../../../../_presets'


function LastTransaction(props) {
    console.log(props)
    let transactionTitle = () => {
        let type = props.event.type
        if (type.toLowerCase() === 'deposit') {
            return 'Пополнение'
        }
        if (type.toLowerCase() === 'withdrawal') {
            return 'Вывод'
        }
    }
    let setIcoType = () => {
        if (props.event.amount.toString()[0] == '-') {
            return arrowDown
        }
        else {
            return arrowUp
        }
    }

    return (
        <div className='last-transaction-item'>
            <img style={{ width: '40px', marginRight: '10px' }} src={setIcoType()} />
            <div className='transaction-time-title-container'>
                <div className='transaction-title'>{transactionTitle()}</div>
                <div style={{ margin: '0', fontSize: '11px' }} className='news-card-date'>{setTransactionTime(props.event.timestamp)}</div>
            </div>
            <div className='transaction-sum'>{props.event.amount}$</div>
        </div>

    )
}
function LastTransactions(props) {

    // if (props.lastEvents !== undefined && props.lastEvents.length > 0) {
    return <section style={{ marginTop: '80px', width: '100%' }} className='last-transactions'>
        <div className="secttion_title">Последнии транзакции фонда</div>
        <div className='transaction-header'>
            <div className=''>Транзакции</div>
            <div className=''>Сумма</div>
        </div>
        {(props.lastEvents.map((currentEvent) => {
            return (<LastTransaction event={currentEvent}
            />)
        }))}
    </section>
    //}
    // else {
    //     return <section style={{ marginTop: '50px', marginBottom: '30px', width: '100%' }} className='last-transactions'>
    //         <div className="secttion_title">Последнии транзакции</div>
    //         <div style={{ fontWeight: 'bold', color: 'grey' }}> Нет транзакций</div>
    //     </section>
    // }
}


export default LastTransactions