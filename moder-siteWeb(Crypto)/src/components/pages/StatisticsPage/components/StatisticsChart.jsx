import { Area } from '@ant-design/charts'
import { setTime } from '../../../../_presets';



function StatisticsChart(props) {

    let data = [];
    let setData = () => {
        if (props.earnings !== undefined) {
            props.earnings.map((current) => {
                data.push({ year: setTime(current.timestamp), value: (current.period_apr), title: (`Доходность за неделю %`) })
                data.push({ year: setTime(current.timestamp), value: (current.year_apr), title: (`Годовая доходность %`) })
            })
        }

    }
    setData()

    const config = {
        data: data,
        grid: null,
        xField: 'year',
        yField: 'value',
        color: ['#13c2c2', ''],
        seriesField: 'title',
        xAxis: {
            fontSize: 100
        },
        point: {
            size: 4,
            shape: 'circle',
        },


        legend: { position: 'bottom' },
    };
    return <Area style={{ width: '100%' }} {...config} />;



}

export default StatisticsChart
