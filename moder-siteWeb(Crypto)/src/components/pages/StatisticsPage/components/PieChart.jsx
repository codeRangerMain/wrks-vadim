import React, { useState, useEffect } from 'react';
import { Pie } from '@ant-design/charts';


const PieChart = () => {
    var data = [
        {
            type: 'Тариф начинающий',
            value: 2000,
        },
        {
            type: 'Тариф продвинутый',
            value: 20000,
        },
        {
            type: 'Тариф про',
            value: 1000,
        },

    ];
    var config = {
        appendPadding: 10,
        data: data,
        angleField: 'value',
        colorField: 'type',
        radius: 0.75,
        label: {
            type: 'spider',
            labelHeight: 28,
            content: '{name}\n{percentage}',
        },
        interactions: [{ type: 'element-selected' }, { type: 'element-active' }],
    };
    return <Pie style={{ width: '100%' }}  {...config} />;
};

export default PieChart;