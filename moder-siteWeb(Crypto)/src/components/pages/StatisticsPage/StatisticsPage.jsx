

import Footer from "../../Footer"
import Header from "../../Header"
import NavHeader from "../../NavHeader"
import LastTransactions from "./components/LastTransactions"
import StatisticsChart from './components/StatisticsChart'
import { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { fetchStatistic } from './statisticsSlice'
import telegramLogo from '../../../img/telegramLogo.svg'
import whatAppLogo from '../../../img/whatAppIco.svg'
import { Modal } from 'antd'
import Loading from "../../Loading"
import { setTime } from "../../../_presets"



function StatisticsPage() {
    const dispatch = useDispatch()
    const allStatistics = useSelector(state => state.statisticsSlice.statisticData.data)
    const isLoading = useSelector(state => state.statisticsSlice.loading)
    const [isModalVisible, setIsModalVisible] = useState(false);

    const showModal = (e) => {
        setIsModalVisible(true);
    };

    const handleOk = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };


    useEffect(() => {
        dispatch(fetchStatistic())
    }, [dispatch])

    const getProfit = () => {
        return (Math.round(100000 * allStatistics.multiplier - 100000))
    }

    return (
        <section className='statisticsPage'>
            <div style={{ marginBottom: '50px', height: '200px' }} className="page_header_bg">
                <Header />
                <NavHeader name="Статистика фонда" path="/" toPage="На главную" />
            </div>
            {!isLoading && allStatistics ? <div style={{ color: '#000' }} className="content_wrapper">
                <h1 className="secttion_title">
                    Статистика доходности фонда
                </h1>
                <StatisticsChart earnings={allStatistics.earnings} />
                <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', textAlign: 'center', width: '100%', marginTop: '53px', border: '2px solid rgb(19, 194, 194)', borderRadius: '10px', padding: '20px', paddingTop: '50px', paddingBottom: '50px', backgroundColor: 'rgb(255, 255, 255)' }}>
                    <div className="statistics_page_profitability">Средняя годовая доходность: {allStatistics.avgApy}%</div>
                    <p className='statistics_page_trap' >При вкладе <b>100 000$</b> в фонд <b>{setTime(allStatistics.earnings[0].timestamp - 604800000)}</b>, к сегодняшнему дню чистая прибыль составила бы <b>{getProfit()}$</b>
                    </p>
                    <a style={{ width: '250px' }} id='showContact' onClick={showModal} className='btn btn-profile'>Связь с менеджером</a>
                </div>
                <Modal cancelText="Закрыть" title="Контакты" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
                    <div className="footer_social">
                        <div style={{ color: 'rgb(133, 143, 149)', marginBottom: '13px' }}>По всем вопросам обращайтесь:
                        </div>
                        <div className="footer_social_item">
                            <img alt='' className="footer_telegram_logo" src={telegramLogo} /><a target="_blank" href='https://t.me/modern_strategies' style={{ color: '#35A2D9' }}>Мы<span style={{ color: '#000' }}> в Telegram </span></a>
                        </div>
                        <div className="footer_social_item">
                            <img alt='' style={{ width: '25px', height: '25px' }} className="footer_telegram_logo" src={whatAppLogo} /><a target="_blank" href='https://wa.me/79017339775' style={{ color: '#35A2D9' }}>Мы <span style={{ color: '#000' }}>в WhatsApp</span></a>
                        </div>
                        <div style={{ color: '#858F95' }} className="footer_social_item">
                            <img alt="" style={{ marginLeft: '3px' }} className="footer_telegram_logo" /><a target="_blank" style={{ color: 'rgb(133, 143, 149)' }} href="mailto:modern.strategies.io@gmail.com">modern.strategies.io@gmail.com</a>
                        </div>
                    </div>
                </Modal>
                <LastTransactions lastEvents={allStatistics.lastDepsWithdrawals} />
            </div> : <Loading />}
            <Footer />
        </section>
    )
}

export default StatisticsPage
