import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { api } from '../../../Store/api'

export const fetchStatistic = createAsyncThunk(
  'statistics/fetchStatistic',
  async () => {
    const response = await api.get('/earningsStatistics')
    return response.data
  }
)

const statisticsSlice = createSlice({
  name: 'statistic',
  initialState: {
    statisticData: [],
    loading: true,
    error: {
      failed: false,
      message: null
    }
  },
  extraReducers: {
    [fetchStatistic.pending]: (state) => {
      state.loading = true
      state.error.failed = false
    },
    [fetchStatistic.fulfilled]: (state, action) => {
      state.loading = false
      state.statisticData = action.payload
    },
    [fetchStatistic.rejected]: (state, action) => {
      state.loading = false
      state.error.failed = true
      state.error.message = action.payload
    }
  }
})

export default statisticsSlice.reducer