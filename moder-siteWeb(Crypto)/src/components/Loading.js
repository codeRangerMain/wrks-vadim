import React from 'react'
import 'antd/dist/antd.css'
import '../App.css';
import { Spin } from 'antd'

function Loading() {
  return (
    <div style={{ paddingTop: '280px', paddingBottom: '300px', width: '100%', display: 'flex', justifyContent: 'center' }}>
      <Spin size="large" />
    </div>
  )
}

export default Loading