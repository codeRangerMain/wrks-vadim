import { Link, scroller } from "react-scroll";
import { NavLink as RouterNavLink, NavLink } from 'react-router-dom'
import { useAuth0 } from "@auth0/auth0-react";
import { useState } from 'react'
import telegramLogo from '../img/telegramLogo.svg'
import whatAppLogo from '../img/whatAppIco.svg'
import { Modal, Select } from 'antd'
import { useTranslation } from 'react-i18next'
const { Option } = Select;

const DesktopNav = function () {
    const { t, i18n } = useTranslation()
    const {
        isAuthenticated,
        loginWithRedirect,
    } = useAuth0();
    const [isModalVisible, setIsModalVisible] = useState(false);

    const showModal = (e) => {
        setIsModalVisible(true);
    };

    const handleOk = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };
    const changeLang = (lang) => {
        i18n.changeLanguage(lang)
        localStorage.setItem('lang', lang)
    }



    const clickHandler = (e) => {
        let target = e.target.innerText
        let scrollTo = ''
        if (target === 'О компании') {
            scrollTo = 'aboutCompany'
        }
        if (target === 'Как это работает') {
            scrollTo = 'howItWork'
        }
        if (target === 'Тарифы') {
            scrollTo = 'tairffs'
        }
        if (target === 'Риски') {
            scrollTo = 'risks'
        }
        if (target === 'Примеры') {
            scrollTo = 'examples'
        }
        // if (target === 'Контакты') {
        //     scrollTo = 'leedForm'
        // }





        setTimeout(() => {
            scroller.scrollTo(scrollTo, {
                duration: 1000,
                smooth: true,
                offset: 0,
                spy: true
            })
        }, 100);
    }
    return (

        <div className="desktop-nav">
            <div className="desktop-nav-container">
                {/* <button onClick={() => changeLang("en")}>En</button>
                <button onClick={() => changeLang("ru")}>Ru</button> */}
                <Link onClick={clickHandler}
                    activeClass="nav-item-active"
                    to=""
                    spy={true}
                    smooth={true}
                    duration={500}
                    className="desktop-menu__item" href="#"><NavLink className='desktop-menu__item' to='/'>{t("howItWorks")}</NavLink></Link>
                <Link onClick={clickHandler}
                    activeClass="nav-item-active"
                    to=""
                    spy={true}
                    smooth={true}
                    duration={500}
                    offset={-50}
                    className="desktop-menu__item" href="#"><NavLink className='desktop-menu__item' to='/'>{t("tariffs")}</NavLink></Link>
                <Link onClick={clickHandler}
                    activeClass="nav-item-active"
                    to=""
                    spy={true}
                    smooth={true}
                    duration={500}
                    className="desktop-menu__item" href="#"><NavLink className='desktop-menu__item' to='/'>{t("examples")}</NavLink></Link>
                <NavLink activeClassName="desktop-menu__item-active" to='/blog' className="desktop-menu__item" >{t("blog")}</NavLink>
                <NavLink activeClassName="desktop-menu__item-active" to='/FAQ' className="desktop-menu__item" >FAQ</NavLink>
                <NavLink activeClassName="desktop-menu__item-active" to='/statistics' className="desktop-menu__item" >{t("statistics")}</NavLink>
                {isAuthenticated && (

                    <NavLink
                        tag={RouterNavLink}
                        to="/ProfilePage"
                        activeClassName="desktop-menu__item-active"
                        className="desktop-menu__item"

                    >
                        {t("myAccount")}
                    </NavLink>
                )}


                {!isAuthenticated && (

                    <a

                        className="desktop-menu__item"
                        onClick={() => loginWithRedirect()}
                    >
                        {t("signIn")}
                    </a>
                )}


                {/* <Select value="РУЗКИЙ">
                    <Option>РУЗКИЙ</Option>
                    <Option >ПЕНДАСКИЙ</Option>
                </Select> */}
                <a onClick={showModal}
                    activeClass="nav-item-active"
                    className="desktop-menu__item desktop-menu__item__registration" href="#">{t("contacts")}</a>
            </div>
            <Modal cancelText="Закрыть" title="Контакты" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
                <div className="footer_social">
                    <div style={{ color: 'rgb(133, 143, 149)', marginBottom: '13px' }}>По всем вопросам обращайтесь:
                    </div>
                    <div className="footer_social_item">
                        <img alt='' className="footer_telegram_logo" src={telegramLogo} /><a target="_blank" href='https://t.me/modern_strategies' style={{ color: '#35A2D9' }}>Мы<span style={{ color: '#000' }}> в Telegram </span></a>
                    </div>
                    <div className="footer_social_item">
                        <img alt='' style={{ width: '25px', height: '25px' }} className="footer_telegram_logo" src={whatAppLogo} /><a target="_blank" href='https://wa.me/79017339775' style={{ color: '#35A2D9' }}>Мы <span style={{ color: '#000' }}>в WhatsApp</span></a>
                    </div>
                    <div style={{ color: '#858F95' }} className="footer_social_item">
                        <img alt="" style={{ marginLeft: '3px' }} className="footer_telegram_logo" /><a target="_blank" style={{ color: 'rgb(133, 143, 149)' }} href="mailto:modern.strategies.io@gmail.com">modern.strategies.io@gmail.com</a>
                    </div>
                </div>
            </Modal>
        </div>
    );
}
export default DesktopNav
