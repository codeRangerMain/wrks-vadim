import { NavLink } from "react-router-dom";
import arrowBack from '../img/arrowBack.svg'
const NavHeader = function (props) {
    return (

        <div className="header-navigation">
            <div><img alt='' style={{ marginRight: '7px' }} src={arrowBack} /><NavLink className="header-navigation-link" to={props.path}>{props.toPage}</NavLink></div>
            <h1 className='page-title'>{props.name}</h1>
        </div>

    );
}
export default NavHeader
