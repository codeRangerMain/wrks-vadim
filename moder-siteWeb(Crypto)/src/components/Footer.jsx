import telegramLogo from '../img/telegramLogo.svg'
import footerLogo from '../img/footerLogo.svg'
import mailLogo from '../img/mail.png'
import whatAppLogo from '../img/whatAppIco.svg'
import { Link, scroller, } from "react-scroll";
import { Element } from 'react-scroll'
import { NavLink } from 'react-router-dom'
import { useTranslation } from 'react-i18next'

const Footer = function () {
    const {t} = useTranslation()

    const clickHandler = (e) => {
        let target = e.target.innerText
        let scrollTo = ''
        if (target === 'О компании') {
            scrollTo = 'aboutCompany'
        }
        if (target === 'Как это работает') {
            scrollTo = 'howItWork'
        }
        if (target === 'Тарифы') {
            scrollTo = 'tairffs'
        }
        if (target === 'Риски') {
            scrollTo = 'risks'
        }
        if (target === 'Примеры') {
            scrollTo = 'examples'
        }
        if (target === 'Контакты') {
            scrollTo = 'leedForm'
        }

        setTimeout(() => {
            scroller.scrollTo(scrollTo, {
                duration: 1000,
                smooth: true,
                offset: 0,
                spy: true
            })
        }, 100);
    }
    return (
        <Element name='footer'>
            <footer className="footer">
                <div className="footer_wrapper">
                    <div className="footer_logo_container">
                        <img alt='' src={footerLogo} />
                        <div className="footer_logo_text">
                            <span style={{ color: '#858F95' }}>Modern</span> Strategies
                        </div>
                    </div>
                    <div className="footer_social">
                        <div style={{ color: 'rgb(133, 143, 149)', marginBottom: '13px' }}>
                            {t("contactsText")}
                        </div>
                        <div className="footer_social_item">
                            <img alt='' className="footer_telegram_logo" src={telegramLogo} /><a target="_blank" href='https://t.me/modern_strategies' style={{ color: '#35A2D9' }}>{t("weAre")}<span style={{ color: '#fff' }}>{t("contactsTelegram")}</span></a>
                        </div>
                        <div className="footer_social_item">
                            <img alt='' style={{ width: '25px', height: '25px' }} className="footer_telegram_logo" src={whatAppLogo} /><a target="_blank" href='https://wa.me/79017339775' style={{ color: '#35A2D9' }}>{t("weAre")} <span style={{ color: '#fff' }}>{t("contactsWhatsApp")}</span></a>
                        </div>
                        <div style={{ color: '#858F95' }} className="footer_social_item">
                            <img alt="" style={{ marginLeft: '3px' }} className="footer_telegram_logo" src={mailLogo} /><a target="_blank" style={{ color: 'rgb(133, 143, 149)' }} href="mailto:modern.strategies.io@gmail.com">modern.strategies.io@gmail.com</a>
                        </div>
                    </div>
                    <div className="footer_nav">
                        <div className="footer_nav_group ">
                            <Link onClick={clickHandler} activeClass="nav-item-active"
                                to=""
                                spy={true}
                                smooth={true}
                                duration={500} className="footer_nav_item"><NavLink to='/' className="footer_nav_item">{t("howItWorks")}</NavLink></Link>
                            <Link onClick={clickHandler} activeClass="nav-item-active"
                                to=""
                                spy={true}
                                smooth={true}
                                duration={500}
                                offset={-50} className="footer_nav_item"><NavLink to='/' className="footer_nav_item">{t("tariffs")}</NavLink></Link>
                        </div>
                        <div className='footer_nav_group '>

                            <Link onClick={clickHandler}
                                activeClass="nav-item-active"
                                to=""
                                spy={true}
                                smooth={true}
                                duration={500}
                                className="footer_nav_item"><NavLink to='/' className="footer_nav_item">{t("examples")}</NavLink></Link>
                            <NavLink to='/blog' className="footer_nav_item">{t("blog")}</NavLink>
                            <NavLink to='/FAQ' className="footer_nav_item">FAQ</NavLink>
                        </div>
                    </div>

                    <div className="footer_copyright">Modern Strategies ©</div>
                </div>
            </footer>
        </Element>
    );
}
export default Footer


