import { configureStore } from '@reduxjs/toolkit'
import earningInfoSlice from '../components/pages/HomePage/components/EarningInfo/earningInfoSlice'
import blogSlice from '../components/pages/BlogPage/blogSlice'
import leedFormSlice from '../components/pages/HomePage/components/LeedForm/leedFormSlice'
import profileSlice from '../components/pages/ProfilePage/profileSlice'
import statisticsSlice from '../components/pages/StatisticsPage/statisticsSlice'
export const store = configureStore({
    reducer: {
        earningInfoSlice,
        blogSlice,
        leedFormSlice,
        profileSlice,
        statisticsSlice
    }
})
