import axios from 'axios';
// // Default config options
let defaultOptions = {
  baseURL: 'https://modernstrategies.io/api/',
  headers: {
    'Content-Type': 'application/json',
  },
};

// Create instance
export const api = axios.create(defaultOptions);

