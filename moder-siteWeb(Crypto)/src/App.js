import './App.css';
import BlogPage from './components/pages/BlogPage/BlogPage';
import HomePage from './components/pages/HomePage/HomePage';
import BlogNewsPage from './components/pages/BlogNewsPage/BlogNewsPage'
import FaqPage from './components/pages/FaqPage/FaqPage'
import NotFoundPage from './components/pages/NotFoundPage/NotFoundPage';
import ProfilePage from './components/pages/ProfilePage/ProfilePage';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { useEffect } from 'react'
import ReactGA from 'react-ga'
import { useAuth0 } from "@auth0/auth0-react";
import LogOut from './components/pages/LogOut'
import StatisticsPage from './components/pages/StatisticsPage/StatisticsPage';


function App() {

  const { error } = useAuth0();


  useEffect(() => {
    ReactGA.initialize('UA-200740095-1')

    ReactGA.pageview(window.location.pathname + window.location.search)
  }, [])

  if (error) {
  }

  return (
    <div className="App">
      <Router>
        <Switch>
          <Route exact path="/" component={HomePage} />
          <Route exact path="/blog" component={BlogPage} />
          <Route exact path="/newsPage/:id?" component={BlogNewsPage} />
          <Route exact path="/FAQ" component={FaqPage} />
          <Route exact path="/ProfilePage" component={ProfilePage} />
          <Route exact path='/LogOut' component={LogOut} />
          <Route exact path='/statistics' component={StatisticsPage} />
          <Route component={NotFoundPage} status={404} />
        </Switch>
      </Router>
    </div>

  );
}



export default App
