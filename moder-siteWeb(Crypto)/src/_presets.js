export let setTime = (timeMil) => {
    let time = new Date(timeMil);
    let date = time.toLocaleDateString()
    return (date)
}

export let setTransactionTime = (mil) => {
    let time = new Date(mil);
    let date = time.toLocaleDateString()
    let hours = time.getHours().toLocaleString()
    let minutes = time.getMinutes().toLocaleString()
    if (minutes.length < 2) {
        minutes = '0' + minutes
    }
    if (hours.length < 2) {
        hours = '0' + hours
    }

    return (hours + ':' + minutes + ' || ' + date)
}