import i18n from 'i18next'
import { initReactI18next} from 'react-i18next'
import translationRU from './locales/ru/translationRU.json'
import translationEN from './locales/en/translationEN.json'

const lang = localStorage.getItem('lang')
const resources = {
  ru: {
    translation: translationRU
  },
  en: {
    translation: translationEN
  }
}
i18n
  .use(initReactI18next)
  .init({
    resources,
    // fallbackLng: lang,
    fallbackLng: 'ru',
    whitelist: ['ru', 'en'],
    detection: {
      order: ["localStorage", "cookie"],
      caches: ["localStorage", "cookie"]
    },
    keySeparator: false,

    interpolation: {
      escapeValue: false
    }
  })
export default i18n