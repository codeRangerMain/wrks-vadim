import React from 'react';
import './i18n';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { Provider } from 'react-redux';
import { store } from './Store/store'
import 'antd/dist/antd.css';
import { getConfig } from './config'
import history from './utils/history'
import { Auth0Provider } from '@auth0/auth0-react'

const onRedirectCallback = (appState) => {
  history.push(
    appState && appState.returnTo ? appState.returnTo : window.location.pathname
  );
};
const config = getConfig();

const providerConfig = {
  domain: config.domain,
  clientId: config.clientId,
  ...(config.audience ? { audience: config.audience } : null),
  redirectUri: `${window.location.origin + '/ProfilePage'}`,
  onRedirectCallback,
};


ReactDOM.render(

  <Provider store={store}>
    <Auth0Provider  {...providerConfig}>
      <App />
    </Auth0Provider>
  </Provider>
  ,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
