export const wsData = [{
    raceType: "qualification",
    localTime: "12:01",
    flag: "green",
    runTime: "00:05",
    runTimeToEnd: "09:55"
},
{
    raceType: "qualification",
    localTime: "12:02",
    flag: "red",
    runTime: "00:06",
    runTimeToEnd: "09:54"
},
{
    raceType: "qualification",
    localTime: "12:03",
    flag: "yellow",
    runTime: "00:07",
    runTimeToEnd: "09:53"
},
{
    id: 1,
    name: "test",
    lap: 5
},
{
    id: 1,
    name: "test ok",
},
{
    id: 1,
    lastLapTime: '26.000'
},
{
    id: 2,
    lastLapTime: '28.000',
    averageTime: '1:12.021',
    lap: 10
},
{
    raceType: "qualification",
    localTime: "12:04",
    flag: "grey",
    runTime: "00:08",
    runTimeToEnd: "11:11"
},
{
    raceType: "qualification",
    localTime: "12:04",
    flag: "finish",
    runTime: "00:09",
    runTimeToEnd: "01:24"
},



]
