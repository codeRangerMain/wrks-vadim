import React from 'react'
import { Provider } from 'react-redux'
import SelectOurTracks from './src/screens/SelectOurTrack/SelectOurTrack'
import { store } from './src/store/store'
import ScoreBoard from './src/screens/ScoreBoard/ScoreBoard'
import ArrivalsArchive from './src/screens/ArrivalsArchive/ArrivalsArchive'
import PersonalInfo from './src/screens/PersonalInfo/PersonalInfo'
import { SafeAreaView } from 'react-native'
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

export default function App() {
  const Stack = createNativeStackNavigator();
  return (

    <Provider store={store}>
      <NavigationContainer>

        <Stack.Navigator initialRouteName="SelectTrack"
          screenOptions={{
            headerShown: false
          }}>
          <Stack.Screen name="SelectTrack" component={SelectOurTracks} />
          <Stack.Screen name="ArrivalsArchive" component={ArrivalsArchive} />
          <Stack.Screen name="ScoreBoard" component={ScoreBoard} />
          <Stack.Screen name="PersonalInfo" component={PersonalInfo} />

        </Stack.Navigator>
      </NavigationContainer>
      {/* <NativeRouter>
          <AnimatedStack
            swipeCancelSpeed={50}
            swipeable={true}
          >
            <Route exact path='/' >
              <SelectOurTracks />
            </Route>
            <Route path='/display/:id?/' >
              <ScoreBoard />
            </Route>
            <Route path='/trackMenu/:id?' component={TrackMenu} />
            <Route path='/personalInfo/:id?' component={PersonalInfo} />
            <Route path='/arrivalsArchive/:id?' component={ArrivalsArchive} />
          </AnimatedStack>
        </NativeRouter> */}
    </Provider >

  )
}





// export default class App extends React.Component {
//   closeControlPanel = () => {
//     this._drawer.close()
//   }
//   openControlPanel = () => {
//     this._drawer.open()
//   }
//   render() {
//     this.openControlPanel()
//     return (
//       <Provider store={store}>
//         <NativeRouter>
//           <Drawer
//             open={true}
//             ref={(ref) => this._drawer = ref}
//           // content={<ControlPanel />}
//           >
//             <Route exact path='/' component={SelectOurTracks} />
//             <Route path='/display:id?' component={ScoreBoard} />
//           </Drawer>
//         </NativeRouter>
//       </Provider>
//     )
//   }
// }
















{/* <Route exact path="/" component={SelectOurTracks} /> */ }