import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import { NavLink, useParams } from 'react-router-dom'
import { useHistory } from "react-router-dom";
import s from './Settings.module.css'
import { changeLettersNameVal, changeCompetitorRows } from '../ScoreBoard/scoreBoardSlice';
import { useDispatch } from 'react-redux';
const ViewSettings = (props) => {

    const dispatch = useDispatch()
    const history = useHistory()
    const currentCompetitorRows = useSelector(state => state.scoreBoardSlice.competitorRows)
    const currentLettersNameVal = useSelector(state => state.scoreBoardSlice.lettersNameVal)
    const [numberOfRows, changeNumberOfRows] = useState(currentCompetitorRows)
    const [numberOfLetters, changeNumberOfLetters] = useState(currentLettersNameVal)
    const changeSettings = () => {
        dispatch(changeLettersNameVal(numberOfLetters))
        dispatch(changeCompetitorRows(numberOfRows))
        history.goBack()
    }
    return (
        <div className={s.form_wrapper}>
            <form name='viewSettings' className={s.form_horizontal}>
                <div className={s.control_group}>
                    <label className={s.control_label} for="inputRows">Количество строк участников:</label>
                    <div className={s.controls}>
                        <input onChange={(e) => { changeNumberOfRows(e.target.value) }} className={s.inputTxt} type="number" id="inputRows" placeholder={currentCompetitorRows}></input>
                    </div>
                </div>
                <div className={s.control_group}>
                    <label className={s.control_label} for="inputWidthPilotColumn">Вмещаемость строки Номер/Пилот:</label>
                    <div className={s.controls}>
                        <input onChange={(e) => { changeNumberOfLetters(e.target.value) }} className={s.inputTxt} type="number" id="inputWidthPilotColumn" placeholder={currentLettersNameVal}></input>
                    </div>
                </div>
                <div className={s.control_group}>
                    <div className={s.controls}>
                        <div style={{ position: 'relative', left: '211px' }}> <a onClick={() => { history.goBack() }} className={s.btnClose} >Закрыть</a> <a onClick={changeSettings} className={s.btn} >Сохранить</a></div>
                    </div>
                </div>
            </form>
        </div >
    )
}


export default ViewSettings

