import axios from 'axios'
import { createSlice, createAsyncThunk, } from "@reduxjs/toolkit"

export const fetchCurrentTrack = createAsyncThunk(
    'track/fetchCurrentTrack',
    async (id) => {
        // const response = (await axios.get(`http://localhost:3000/${id}`)).data
        const response = (await axios.get(`http://185.147.83.196/run/get?plantId=1`)).data
        return response
    }
)

const scoreBoardSlice = createSlice({
    name: "track",
    initialState: {
        runData: {
            run: {},
            competitors: []
        },
        loaded: false,
        isLive: false,
        zoom: null,
        competitorRows: 5,
        lettersNameVal: 5,
        nameColumnWidth: 5,
    },
    reducers: {
        changeRaceData(state, action) {
            let payload = JSON.parse(action.payload)
            console.log(payload)
            if (payload.type == 'RunHeartBeated') {
                state.runData.run = Object.assign(state.runData.run, payload.data)
            }
            if (payload.type === 'CompetitorAdded') {
                state.runData.competitors.push(payload.data)
            }
            // if (state.runData.competitors && state.runData.run && action.payload) {
            //     if (action.payload.raceType) {
            //         state.runData.run = action.payload
            //     }

            //     else if (action.payload.id) {
            //         let competitorIndex = state.runData.competitors.findIndex(competitor => competitor.id === action.payload.id)
            //         if (competitorIndex !== -1) {
            //             state.runData.competitors[competitorIndex] = Object.assign(state.runData.competitors[competitorIndex], action.payload)
            //         }
            //         else {
            //             console.log('Не могу найти участника с id: ', action.payload.id)
            //         }
            //     }
            // }
            // else {
            //     console.log(('ERROR: data is undefined '))
            // }
        },
        setZoom(state) {
            if (window.innerWidth > 1028) {
                let rowHeight = 36.7
                let competitorsNum = state.competitorRows
                let headerHeight = document.querySelector('.Header').offsetHeight + document.querySelector('.competitors-header').offsetHeight
                let appHeight = (rowHeight * (competitorsNum)) + headerHeight
                let windowHeight = window.innerHeight
                let windowCurrentHeight = Math.round(windowHeight)
                let pageZoom = (windowCurrentHeight / appHeight)
                state.zoom = pageZoom
            }
        },
        changeLettersNameVal(state, action) {
            state.lettersNameVal = action.payload
        },
        changeCompetitorRows(state, action) {
            state.competitorRows = action.payload
        },
        setNameColumnWidth(state) {
            const fontSize = 16
            let letterWidth = fontSize / 2
            let wordWidth = letterWidth * state.lettersNameVal
            state.nameColumnWidth = wordWidth / fontSize + 3.2 + 'em'
        },
        setScoreBoardData(state, action) {
            state.runData = action.payload
            console.log(action.payload.run.type)
            // if (action.payload.run.type === 'race') {
            //     action.payload.del
            // }
            state.loaded = true
        }
    },
    extraReducers: {
        [fetchCurrentTrack.pending]: (state) => {
            state.isLive = true
            state.loaded = false
            console.log('Запрашиваю данные fetchCurrentTrack')
        },
        [fetchCurrentTrack.fulfilled]: (state, action) => {
            state.loaded = true
            state.runData = action.payload
            console.log(action.payload)
            console.log('Получил данные данные fetchCurrentTrack')
        },
        [fetchCurrentTrack.rejected]: (state, action) => {
            state.isLive = false
            console.log('ERROR:', action.payload)
        },

    }
})
export const { changeRaceData, setScoreBoardData, setZoom, setNameColumnWidth, changeLettersNameVal, changeCompetitorRows } = scoreBoardSlice.actions
export default scoreBoardSlice.reducer
