import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { changeRaceData, setNameColumnWidth, setZoom, fetchCurrentTrack } from './scoreBoardSlice';
import Competitor from './components/Competitor';
import Header, { CompetitorsHeader } from './components/Header';
import Menu from './components/Menu';
import { useParams } from 'react-router';
import { HubConnection, HubConnectionBuilder } from "@microsoft/signalr";

const ScoreBoard = () => {
    const id = useParams().id
    const dispatch = useDispatch()
    const isLoaded = useSelector(state => state.scoreBoardSlice.loaded)
    const [connection, setConnection] = useState(null);
    const runData = useSelector(state => state.scoreBoardSlice.runData)
    const zoom = useSelector(state => state.scoreBoardSlice.zoom)
    const [menuOpen, setMenuOpen] = useState(false)

    const menuToggle = () => {
        setMenuOpen(!menuOpen)
        console.log(menuOpen)
    }

    useEffect(() => {
        if (isLoaded) {
            dispatch(setZoom())
            dispatch(setNameColumnWidth())
        }
    }, [isLoaded])

    useEffect(() => {
        const connect = new HubConnectionBuilder()
            .withUrl("http://185.147.83.196/telemetry", {
                accessTokenFactory: () => 'ABCDEF'
            })
            .withAutomaticReconnect()
            .build();

        setConnection(connect);
    }, []);

    useEffect(() => {
        console.log(connection)
        if (connection) {
            connection.start().then(() => {
                connection.send("StartReceiveTelemetry", 1)
                dispatch(fetchCurrentTrack(id))
                connection.on("TelemetryChanged", (msg) => {
                    dispatch(changeRaceData(msg))
                });
                connection.send("SendTelemetry",
                    {
                        id: 1,
                        message: "$I,\"13:08:45.000\",\"02 íîÿ 19\"",
                        eventDate: Date.now().toString()
                    });
            })
                .catch((error) => console.log(error, ' ERROR'))
        }
    }, [connection, isLoaded]);

    const competitors = runData && isLoaded ? runData.competitors.map((currentCompetitor, index) => {
        return (<Competitor index={index} currentLap={index + 1} currentCompetitor={currentCompetitor} />)
    }) : null

    return (
        <div className="Display" onClick={menuToggle} style={{ zoom: zoom }} >
            <div className='scoreboard-template- scoreboard-order-bestlap' id='scoreboard'>
                <Menu isOpen={menuOpen} />
                <Header />
                <div className='competitors'>
                    <CompetitorsHeader />
                    <div className='competitors-body'>
                        {competitors}
                    </div>
                </div>
            </div>
        </div >
    );
}
export default ScoreBoard