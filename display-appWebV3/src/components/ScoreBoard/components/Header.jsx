import React from 'react'
import { useSelector } from 'react-redux'
export const CompetitorsHeader = (props) => {

    const nameColumnWidth = useSelector(state => state.scoreBoardSlice.nameColumnWidth)

    return (
        <div className='competitors-header'>
            <div className="position">
                <span>Поз</span>
            </div>
            <div className="name" style={{ width: nameColumnWidth }}>
                <span>Номер ⁄ Пилот</span>
            </div>
            <div className="laps">
                <span>Кругов</span>
            </div>
            <div className="lastlap">
                <span>Последний</span>
            </div>
            <div className="bestlap">
                <span>Лучший</span>
            </div>
            <div className="diff-bestlap">
                <span>Разница</span>
            </div>

        </div >
    )
}

const Header = (props) => {
    const runInfo = useSelector(state => state.scoreBoardSlice.runData.run)
    return (
        <div className="Header">
            {runInfo ? <div className='header'> <div className="runNumber">
                <div className={"flag flag-" + (runInfo.flag !== 'None' && runInfo.flag ? runInfo.flag.toLowerCase() : 'gray')}>
                </div>
                {runInfo.name ? runInfo.name : 'Заезд №' + runInfo.id}
            </div>
                <div className="runTime">{runInfo.runTime ? runInfo.runTime.slice(3, runInfo.runTime.length) : null} ({runInfo.timeToEnd ? runInfo.timeToEnd.slice(3, runInfo.timeToEnd.length) : '00:00'})</div>
                <div className="clock">{runInfo.localTime}</div>
            </div> : null}
        </div>

    )
}

export default Header