import React from 'react'
import { NavLink } from 'react-router-dom'

const Menu = (props) => {

    return (
        <div className={'scoreboard-menu ' + (!props.isOpen ? 'hide' : '')}>
            <div className="menu-element">
                <NavLink to={"/Display/" + 1 + "/view_settings"} href='#'>  Настройки отображения</NavLink>
            </div>
            <div className="menu-element">
                <NavLink to="/" href='#'> Выбор картодрома</NavLink>
            </div>
            <div onClick={props.menuVisability} className="menu-element">
                <a>Закрыть</a>
            </div>
        </div >
    )
}
export default Menu