import React from 'react'
import { useSelector } from 'react-redux'
const Competitor = (props) => {
    const currentCompetitor = props.currentCompetitor
    const lap = props.currentLap
    const nameColumnWidth = useSelector(state => state.scoreBoardSlice.nameColumnWidth)
    return (
        <div className={'row '} >
            {/* {currentCompetitor.lastlap */}
            <div className="position">{currentCompetitor.stats.positionByDistance}</div>
            <div style={{ width: nameColumnWidth }} className="name">
                <span className="number">{currentCompetitor.info.id}</span>
                <span className="span-name">{currentCompetitor.info.name}</span>
                <span className="laps-mobail">{'1'} </span>
            </div>
            <div className="laps">
                <span>{lap}</span>
                <span className="hide-desktop"></span>
            </div>
            <div className={"lastlap "}>{"+\u2009" + currentCompetitor.stats.lastLapTime}</div>
            <div className={"bestlap "}>{"+\u2009" + currentCompetitor.stats.bestLapTime}
                <span className="diff-bestlap-mobail">{'21'}</span>
            </div>
            <div className="diff-bestlap"><span>{"+\u2009" + currentCompetitor.stats.diffByDistance}</span>
            </div>
            <div className="competitor-mark" ></div>
//}
        </div >
    )
}
export default Competitor;