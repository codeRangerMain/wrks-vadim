import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { fetchAllTracks } from './selectDisplaySlice'
import DisplayItem from './components/DisplayItem';

const SelectDisplay = () => {
    const dispatch = useDispatch()
    const displaysData = useSelector(state => state.selectDisplaySlice.allTracks)

    useEffect(() => {
        dispatch(fetchAllTracks())
    }, [])

    let Displays = displaysData.map((currentDisplay, index) => {
        return (<DisplayItem key={currentDisplay.id + index} displayInfo={currentDisplay}
        />)
    })

    return (
        <div className="ChooseDisplay">
            {Displays}
        </div >
    );
}
export default SelectDisplay
