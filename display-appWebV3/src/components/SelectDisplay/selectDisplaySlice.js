import axios from 'axios'
import { createSlice, createAsyncThunk, } from "@reduxjs/toolkit"

export const fetchAllTracks = createAsyncThunk(
    'track/fetchAllTracks',
    async () => {
        const response = (await axios.get(`http://localhost:3000/tracks`)).data
        return response
    }
)

const selectDisplaySlice = createSlice({
    name: "tracks",
    initialState: {
        allTracks: [],
        loaded: false,
        error: ''
    },
    reducers: {
    },
    extraReducers: {
        [fetchAllTracks.pending]: (state) => {
            state.loaded = false
            console.log('Запрашиваю данные fetchAllTracks')
        },
        [fetchAllTracks.fulfilled]: (state, action) => {
            state.loaded = true
            state.allTracks = action.payload
            console.log('Получил данные данные fetchAllTracks')
        },
        [fetchAllTracks.rejected]: (state, action) => {
            console.log('ERROR:', action.payload)
            state.error = action.payload
        },
    }
})

export default selectDisplaySlice.reducer
