import React from 'react'
import { useDispatch } from 'react-redux'
import { NavLink } from 'react-router-dom'
import { changeCompetitorRows } from '../../ScoreBoard/scoreBoardSlice'
//Дисплей компонент 
const DisplayItem = (props) => {
    const dispatch = useDispatch()
    const displayInfo = props.displayInfo

    let Displays = displayInfo ? displayInfo.displays.map((currentDisplay, index) => {
        const clickHandler = () => {
            dispatch(changeCompetitorRows(currentDisplay.settings.rows))
        }
        return (<NavLink key={currentDisplay.name + index} onClick={clickHandler} className='btn btn-choose-display' to={'/display/' + displayInfo.id}>{currentDisplay.name}</NavLink>)
    }) : null

    return (
        <div className='display-item'>
            <div className='display-item-title'>
                {displayInfo.name}
            </div>
            {Displays}
        </div>
    );
}
export default DisplayItem




