import { configureStore } from '@reduxjs/toolkit';
import scoreBoardSlice from '../components/ScoreBoard/scoreBoardSlice';
import selectDisplaySlice from '../components/SelectDisplay/selectDisplaySlice'

export const store = configureStore({
    reducer: {
        scoreBoardSlice,
        selectDisplaySlice
    }
});