import './App.css';
import ScoreBoard from './components/ScoreBoard/ScoreBoard';
import { Provider } from 'react-redux'
import { store } from './store/store'
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import ViewSettings from './components/ViewSettings/ViewSettings';
import SelectDisplay from './components/SelectDisplay/SelectDisplay';
function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Switch>
          <Route exact path='/' render={() => <SelectDisplay />} />
          <Route exact path='/Display/:id?' render={() => <ScoreBoard />} />
          <Switch>
            <Route path='/Display/:id?/view_settings' render={() => <ViewSettings />} />
          </Switch>
        </Switch>
      </BrowserRouter>
    </Provider>
  );
}

export default App;
